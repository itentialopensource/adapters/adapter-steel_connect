
## 0.7.1 [09-11-2023]

* Revert "More migration changes"

See merge request itentialopensource/adapters/sd-wan/adapter-steel_connect!12

---

## 0.7.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/sd-wan/adapter-steel_connect!9

---

## 0.6.5 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/sd-wan/adapter-steel_connect!8

---

## 0.6.4 [07-10-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-steel_connect!7

---

## 0.6.3 [01-15-2020] & 0.6.2 [11-25-2019]

- Fix the path variables -- this meant fixing the swagger and regenerating the adapter. Due to path variables being exposed, many of the adapter method signatures changed.

See merge request itentialopensource/adapters/sd-wan/adapter-steel_connect!6

---

## 0.6.1 [11-18-2019]

- Fix the healthcheck entitypath to something that has been tested

See merge request itentialopensource/adapters/sd-wan/adapter-steel_connect!4

---

## 0.6.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)
  - See merge request itentialopensource/adapters/sd-wan/adapter-steel_connect!3

---

## 0.5.0 [09-16-2019]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-steel_connect!2

---

## 0.4.0 [09-16-2019] & 0.3.0 [07-30-2019] & 0.2.0 [07-17-2019]

- Migration to the latest foundation, categorization and preparation for App Artifact

See merge request itentialopensource/adapters/sd-wan/adapter-steel_connect!1

---

## 0.1.1 [04-26-2019]

- Initial Commit

See commit 1fd543f

---
