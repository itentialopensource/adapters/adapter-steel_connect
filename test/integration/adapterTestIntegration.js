/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-steel_connect',
      type: 'SteelConnect',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const SteelConnect = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Steel_connect Adapter Test', () => {
  describe('SteelConnect Class Tests', () => {
    const a = new SteelConnect(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2.8, data.response.scm_version);
                assert.equal('string', data.response.scm_build);
                assert.equal('object', typeof data.response.fw_versions);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Status', 'getStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationPostOrgsBodyParam = {
      name: 'Examplecorp',
      city: 'New York',
      longname: 'The example corporation'
    };
    describe('#postOrgs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgs(organizationPostOrgsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SDDEDGERTSDGAGASG', data.response.id);
                assert.equal('Examplecorp', data.response.name);
                assert.equal('Examplecorp, Root Road 14, 12345 Adminten', data.response.street_address);
                assert.equal('New York', data.response.city);
                assert.equal('root@examplecorp.com, Mrs Alice Example, Root Road 14, 12345 Adminten', data.response.contact);
                assert.equal(5, data.response.uid);
                assert.equal('realm', data.response.realm);
                assert.equal('string', data.response.gid);
                assert.equal('The example corporation', data.response.longname);
                assert.equal('DE', data.response.country);
                assert.equal('Europe/Berlin', data.response.timezone);
                assert.equal('takes 1 or 0', data.response.ssh);
                assert.equal('ssh-rsa AAAAB3N...zaC1yc2EAAAA user@host', data.response.ssh_keys);
                assert.equal('tjKmC7bEOheH', data.response.console_login_pw);
                assert.equal('take 1 or 0.', data.response.maint_override_realm);
                assert.equal('1 will configure Monday and 7 will configure Sunday.', data.response.maint_window_wday);
                assert.equal('takes in time "04:00" or "16:30".', data.response.maint_window_time);
                assert.equal('takes 1 or 0.', data.response.maint_immediate);
                assert.equal('string', data.response.wan_opt);
                assert.equal(true, Array.isArray(data.response.wans));
                assert.equal('takes an integer like 1000', data.response.vlan_base);
                assert.equal('takes an IPv4 net pool like 172.16.0.0/12', data.response.ipv4_netpool);
                assert.equal('takes an IPv6 net pool like fd00:ced0:ced0::/48', data.response.ipv6_netpool);
                assert.equal('string', data.response.ad_bindpass);
                assert.equal('string', data.response.ad_binduser);
                assert.equal('intranet.company.com', data.response.ad_domain);
                assert.equal('OU=Staff', data.response.ad_ldap_base);
                assert.equal('(&(objectClass=user)(objectCategory=person))', data.response.ad_ldap_filter);
                assert.equal('(objectCategory=group)', data.response.ad_ldap_group_filter);
                assert.equal('ldaps://example.com:636', data.response.ad_ldap_server);
                assert.equal('bridge', data.response.ad_mode);
                assert.equal('node-024197ce83988201', data.response.ad_sync_node);
                assert.equal(false, data.response.dir_live);
                assert.equal('string', data.response.dir_type);
                assert.equal('string', data.response.facebook_application_id);
                assert.equal('string', data.response.facebook_application_secret);
                assert.equal('string', data.response.google_api_client_id);
                assert.equal('string', data.response.google_api_client_secret);
                assert.equal('acme-inc.com', data.response.google_api_domain);
                assert.equal('string', data.response.google_api_iss);
                assert.equal('string', data.response.google_api_pem_private_key);
                assert.equal('string', data.response.google_api_prn);
                assert.equal('string', data.response.twitter_api_consumer_key);
                assert.equal('string', data.response.twitter_api_consumer_secret);
                assert.equal('string', data.response.walled_garden_extra);
                assert.equal(true, Array.isArray(data.response.breakout_preference));
                assert.equal('This takes a site ID.', data.response.breakout_sitelink_site);
                assert.equal(true, data.response.legal_disclaimer_state);
                assert.equal('string', data.response.legal_disclaimer_text);
                assert.equal('string', data.response.ntp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'postOrgs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationOrgid = 'fakedata';
    const organizationPutOrgOrgidBodyParam = {
      name: 'Examplecorp',
      city: 'New York',
      longname: 'The example corporation'
    };
    describe('#putOrgOrgid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putOrgOrgid(organizationOrgid, organizationPutOrgOrgidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'putOrgOrgid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgid(organizationOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SDDEDGERTSDGAGASG', data.response.id);
                assert.equal('Examplecorp', data.response.name);
                assert.equal('Examplecorp, Root Road 14, 12345 Adminten', data.response.street_address);
                assert.equal('New York', data.response.city);
                assert.equal('root@examplecorp.com, Mrs Alice Example, Root Road 14, 12345 Adminten', data.response.contact);
                assert.equal(4, data.response.uid);
                assert.equal('realm', data.response.realm);
                assert.equal('string', data.response.gid);
                assert.equal('The example corporation', data.response.longname);
                assert.equal('DE', data.response.country);
                assert.equal('Europe/Berlin', data.response.timezone);
                assert.equal('takes 1 or 0', data.response.ssh);
                assert.equal('ssh-rsa AAAAB3N...zaC1yc2EAAAA user@host', data.response.ssh_keys);
                assert.equal('tjKmC7bEOheH', data.response.console_login_pw);
                assert.equal('take 1 or 0.', data.response.maint_override_realm);
                assert.equal('1 will configure Monday and 7 will configure Sunday.', data.response.maint_window_wday);
                assert.equal('takes in time "04:00" or "16:30".', data.response.maint_window_time);
                assert.equal('takes 1 or 0.', data.response.maint_immediate);
                assert.equal('string', data.response.wan_opt);
                assert.equal(true, Array.isArray(data.response.wans));
                assert.equal('takes an integer like 1000', data.response.vlan_base);
                assert.equal('takes an IPv4 net pool like 172.16.0.0/12', data.response.ipv4_netpool);
                assert.equal('takes an IPv6 net pool like fd00:ced0:ced0::/48', data.response.ipv6_netpool);
                assert.equal('string', data.response.ad_bindpass);
                assert.equal('string', data.response.ad_binduser);
                assert.equal('intranet.company.com', data.response.ad_domain);
                assert.equal('OU=Staff', data.response.ad_ldap_base);
                assert.equal('(&(objectClass=user)(objectCategory=person))', data.response.ad_ldap_filter);
                assert.equal('(objectCategory=group)', data.response.ad_ldap_group_filter);
                assert.equal('ldaps://example.com:636', data.response.ad_ldap_server);
                assert.equal('bridge', data.response.ad_mode);
                assert.equal('node-024197ce83988201', data.response.ad_sync_node);
                assert.equal(false, data.response.dir_live);
                assert.equal('string', data.response.dir_type);
                assert.equal('string', data.response.facebook_application_id);
                assert.equal('string', data.response.facebook_application_secret);
                assert.equal('string', data.response.google_api_client_id);
                assert.equal('string', data.response.google_api_client_secret);
                assert.equal('acme-inc.com', data.response.google_api_domain);
                assert.equal('string', data.response.google_api_iss);
                assert.equal('string', data.response.google_api_pem_private_key);
                assert.equal('string', data.response.google_api_prn);
                assert.equal('string', data.response.twitter_api_consumer_key);
                assert.equal('string', data.response.twitter_api_consumer_secret);
                assert.equal('string', data.response.walled_garden_extra);
                assert.equal(true, Array.isArray(data.response.breakout_preference));
                assert.equal('This takes a site ID.', data.response.breakout_sitelink_site);
                assert.equal(false, data.response.legal_disclaimer_state);
                assert.equal('string', data.response.legal_disclaimer_text);
                assert.equal('string', data.response.ntp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrgOrgid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgs((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrgs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bgpneighOrgid = 'fakedata';
    describe('#postOrgOrgidBgpneighs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidBgpneighs(bgpneighOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('org-acme-0c5027cd98540e76', data.response.org);
                assert.equal('node-faece15f745e08a0', data.response.node);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.ipv4);
                assert.equal(2, data.response.remote_as);
                assert.equal('string', data.response.password);
                assert.equal(5, data.response.keepalive_time);
                assert.equal(9, data.response.hold_time);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgpneigh', 'postOrgOrgidBgpneighs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBgpneighs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBgpneighs((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgpneigh', 'getBgpneighs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bgpneighBgpneighid = 'fakedata';
    describe('#putBgpneighsBgpneighid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putBgpneighsBgpneighid(bgpneighBgpneighid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgpneigh', 'putBgpneighsBgpneighid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBgpneighsBgpneighid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBgpneighsBgpneighid(bgpneighBgpneighid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('org-acme-0c5027cd98540e76', data.response.org);
                assert.equal('node-faece15f745e08a0', data.response.node);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.ipv4);
                assert.equal(5, data.response.remote_as);
                assert.equal('string', data.response.password);
                assert.equal(5, data.response.keepalive_time);
                assert.equal(7, data.response.hold_time);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgpneigh', 'getBgpneighsBgpneighid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidBgpneighs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidBgpneighs(bgpneighOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgpneigh', 'getOrgOrgidBgpneighs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcinterfaceOrgid = 'fakedata';
    describe('#postOrgOrgidDcinterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidDcinterfaces(dcinterfaceOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('org-acme-0c5027cd98540e76', data.response.org);
                assert.equal('port-LAN1-faece15f745e08a0', data.response.port);
                assert.equal('string', data.response.gateway_ipv4);
                assert.equal('string', data.response.gateway_ipv6);
                assert.equal('string', data.response.ipv4);
                assert.equal('string', data.response.ipv6);
                assert.equal(10, data.response.mtu);
                assert.equal(true, data.response.auto_negotiation);
                assert.equal(true, data.response.enabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcinterface', 'postOrgOrgidDcinterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcinterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcinterfaces((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcinterface', 'getDcinterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcinterfaceDcinterfaceid = 'fakedata';
    describe('#putDcinterfacesDcinterfaceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcinterfacesDcinterfaceid(dcinterfaceDcinterfaceid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcinterface', 'putDcinterfacesDcinterfaceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcinterfacesDcinterfaceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcinterfacesDcinterfaceid(dcinterfaceDcinterfaceid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('org-acme-0c5027cd98540e76', data.response.org);
                assert.equal('port-LAN1-faece15f745e08a0', data.response.port);
                assert.equal('string', data.response.gateway_ipv4);
                assert.equal('string', data.response.gateway_ipv6);
                assert.equal('string', data.response.ipv4);
                assert.equal('string', data.response.ipv6);
                assert.equal(2, data.response.mtu);
                assert.equal(true, data.response.auto_negotiation);
                assert.equal(false, data.response.enabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcinterface', 'getDcinterfacesDcinterfaceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidDcinterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidDcinterfaces(dcinterfaceOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcinterface', 'getOrgOrgidDcinterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uplinkOrgid = 'fakedata';
    const uplinkPostOrgOrgidUplinksBodyParam = {
      site: 'site-HQ-2564a86cdcee8b32',
      wan: 'wan-Internet-b47d7c36130a5ccf'
    };
    describe('#postOrgOrgidUplinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidUplinks(uplinkOrgid, uplinkPostOrgOrgidUplinksBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('uplink-Uplink-8c22c3e3d80f11e1', data.response.id);
                assert.equal('site-HQ-2564a86cdcee8b32', data.response.site);
                assert.equal('wan-Internet-b47d7c36130a5ccf', data.response.wan);
                assert.equal('org-example-8a882293bfddffaf', data.response.org);
                assert.equal(6, data.response.qos_bw_up);
                assert.equal(10, data.response.qos_up);
                assert.equal('string', data.response.static_ip_v4);
                assert.equal('string', data.response.static_gw_v4);
                assert.equal('string', data.response.static_ip_v6);
                assert.equal('string', data.response.static_gw_v6);
                assert.equal(5, data.response.uin);
                assert.equal(3, data.response.uid);
                assert.equal('string', data.response.node);
                assert.equal('string', data.response.name);
                assert.equal(7, data.response.qos_bw_down);
                assert.equal(2, data.response.qos_down);
                assert.equal('string', data.response.port);
                assert.equal(2, data.response.vlan);
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.backup);
                assert.equal('string', data.response.sitelink_ipsel);
                assert.equal('string', data.response.sitelink_custom_ip);
                assert.equal(8, data.response.sitelink_prio);
                assert.equal('[1.2.3.4,5.7.8.9]', data.response.bgp_advertised_routes);
                assert.equal('[1.2.3.4,5.7.8.9]', data.response.bgp_learned_routes);
                assert.equal(2, data.response.bgp_enable);
                assert.equal('1.2.3.4', data.response.bgp_router_id);
                assert.equal('string', data.response.bgp_local_as);
                assert.equal('BGP_NEIGHBOUR', data.response.bgp_neigh_name);
                assert.equal(7, data.response.bgp_neigh_remote_as);
                assert.equal('1.2.3.4', data.response.bgp_neigh_ipv4);
                assert.equal('PASSWORD', data.response.bgp_neigh_password);
                assert.equal('60', data.response.bgp_neigh_keepalive_time);
                assert.equal('180', data.response.bgp_neigh_hold_time);
                assert.equal(8, data.response.mtu);
                assert.equal('string', data.response.qos_inbound);
                assert.equal('string', data.response.qos_inbound_Bandwidth);
                assert.equal('string', data.response.qos_inbound_units);
                assert.equal('string', data.response.qos_outbound);
                assert.equal('string', data.response.qos_outbound_Bandwidth);
                assert.equal('string', data.response.qos_outbound_units);
                assert.equal(false, data.response.auto_negotiation);
                assert.equal(1, data.response.link_speed);
                assert.equal('string', data.response['PPP-reconnect']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Uplink', 'postOrgOrgidUplinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidUplinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidUplinks(uplinkOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Uplink', 'getOrgOrgidUplinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uplinkSiteid = 'fakedata';
    describe('#getSiteSiteidUplinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteSiteidUplinks(uplinkSiteid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Uplink', 'getSiteSiteidUplinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uplinkUplinkid = 'fakedata';
    describe('#putUplinkUplinkid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putUplinkUplinkid(uplinkUplinkid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Uplink', 'putUplinkUplinkid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUplinkUplinkid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUplinkUplinkid(uplinkUplinkid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('uplink-Uplink-8c22c3e3d80f11e1', data.response.id);
                assert.equal('site-HQ-2564a86cdcee8b32', data.response.site);
                assert.equal('wan-Internet-b47d7c36130a5ccf', data.response.wan);
                assert.equal('org-example-8a882293bfddffaf', data.response.org);
                assert.equal(5, data.response.qos_bw_up);
                assert.equal(1, data.response.qos_up);
                assert.equal('string', data.response.static_ip_v4);
                assert.equal('string', data.response.static_gw_v4);
                assert.equal('string', data.response.static_ip_v6);
                assert.equal('string', data.response.static_gw_v6);
                assert.equal(6, data.response.uin);
                assert.equal(1, data.response.uid);
                assert.equal('string', data.response.node);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.qos_bw_down);
                assert.equal(8, data.response.qos_down);
                assert.equal('string', data.response.port);
                assert.equal(10, data.response.vlan);
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.backup);
                assert.equal('string', data.response.sitelink_ipsel);
                assert.equal('string', data.response.sitelink_custom_ip);
                assert.equal(7, data.response.sitelink_prio);
                assert.equal('[1.2.3.4,5.7.8.9]', data.response.bgp_advertised_routes);
                assert.equal('[1.2.3.4,5.7.8.9]', data.response.bgp_learned_routes);
                assert.equal(7, data.response.bgp_enable);
                assert.equal('1.2.3.4', data.response.bgp_router_id);
                assert.equal('string', data.response.bgp_local_as);
                assert.equal('BGP_NEIGHBOUR', data.response.bgp_neigh_name);
                assert.equal(6, data.response.bgp_neigh_remote_as);
                assert.equal('1.2.3.4', data.response.bgp_neigh_ipv4);
                assert.equal('PASSWORD', data.response.bgp_neigh_password);
                assert.equal('60', data.response.bgp_neigh_keepalive_time);
                assert.equal('180', data.response.bgp_neigh_hold_time);
                assert.equal(7, data.response.mtu);
                assert.equal('string', data.response.qos_inbound);
                assert.equal('string', data.response.qos_inbound_Bandwidth);
                assert.equal('string', data.response.qos_inbound_units);
                assert.equal('string', data.response.qos_outbound);
                assert.equal('string', data.response.qos_outbound_Bandwidth);
                assert.equal('string', data.response.qos_outbound_units);
                assert.equal(true, data.response.auto_negotiation);
                assert.equal(7, data.response.link_speed);
                assert.equal('string', data.response['PPP-reconnect']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Uplink', 'getUplinkUplinkid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uplinkPutUplinkUplinkidStateBodyParam = {
      state: 'on'
    };
    describe('#putUplinkUplinkidState - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putUplinkUplinkidState(uplinkUplinkid, uplinkPutUplinkUplinkidStateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Uplink', 'putUplinkUplinkidState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUplinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUplinks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Uplink', 'getUplinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteOrgid = 'fakedata';
    const sitePostOrgOrgidSitesBodyParam = {
      name: 'HQ',
      longname: 'string',
      city: 'New York'
    };
    describe('#postOrgOrgidSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidSites(siteOrgid, sitePostOrgOrgidSitesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('HQ', data.response.name);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.longname);
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal('Examplecorp, Root Road 14, 12345 Adminten', data.response.street_address);
                assert.equal('New York', data.response.city);
                assert.equal('DE', data.response.country);
                assert.equal('Europe/Berlin', data.response.timezone);
                assert.equal(8, data.response.size);
                assert.equal(8, data.response.uid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'postOrgOrgidSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidSites(siteOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'getOrgOrgidSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteSiteid = 'fakedata';
    describe('#putSiteSiteid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSiteSiteid(siteSiteid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'putSiteSiteid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSiteid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteSiteid(siteSiteid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('HQ', data.response.name);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.longname);
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal('Examplecorp, Root Road 14, 12345 Adminten', data.response.street_address);
                assert.equal('New York', data.response.city);
                assert.equal('DE', data.response.country);
                assert.equal('Europe/Berlin', data.response.timezone);
                assert.equal(3, data.response.size);
                assert.equal(7, data.response.uid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'getSiteSiteid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSites((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'getSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sitegroupOrgid = 'fakedata';
    const sitegroupPostOrgOrgidSitegroupsBodyParam = {
      name: 'CityName',
      members: [
        'string'
      ],
      type: 'multi_hub'
    };
    describe('#postOrgOrgidSitegroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidSitegroups(sitegroupOrgid, sitegroupPostOrgOrgidSitegroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('CityName', data.response.name);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal(true, Array.isArray(data.response.sitegroup_leafs));
                assert.equal('multi_hub', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sitegroup', 'postOrgOrgidSitegroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidSitegroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidSitegroups(sitegroupOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sitegroup', 'getOrgOrgidSitegroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sitegroupSitegroupid = 'fakedata';
    describe('#putSitegroupSitegroupid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSitegroupSitegroupid(sitegroupSitegroupid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sitegroup', 'putSitegroupSitegroupid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitegroupSitegroupid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSitegroupSitegroupid(sitegroupSitegroupid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('CityName', data.response.name);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal(true, Array.isArray(data.response.sitegroup_leafs));
                assert.equal('multi_hub', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sitegroup', 'getSitegroupSitegroupid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitegroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSitegroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sitegroup', 'getSitegroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zoneOrgid = 'fakedata';
    const zonePostOrgOrgidZonesBodyParam = {
      name: 'LAN',
      site: 'site-HQ-986eec1d96090b93'
    };
    describe('#postOrgOrgidZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidZones(zoneOrgid, zonePostOrgOrgidZonesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('LAN', data.response.name);
                assert.equal('site-HQ-986eec1d96090b93', data.response.site);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal('string', data.response.mgmt);
                assert.equal('all', data.response.icmp);
                assert.equal('string', data.response.guest);
                assert.equal(true, Array.isArray(data.response.breakout_preference));
                assert.equal(true, Array.isArray(data.response.routes));
                assert.equal(true, Array.isArray(data.response.bcasts));
                assert.equal(1000, data.response.tag);
                assert.equal('string', data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zone', 'postOrgOrgidZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidZones(zoneOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zone', 'getOrgOrgidZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zoneSiteid = 'fakedata';
    describe('#getSiteSiteidZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteSiteidZones(zoneSiteid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zone', 'getSiteSiteidZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zoneZoneid = 'fakedata';
    describe('#putZoneZoneid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putZoneZoneid(zoneZoneid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zone', 'putZoneZoneid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneZoneid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getZoneZoneid(zoneZoneid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('LAN', data.response.name);
                assert.equal('site-HQ-986eec1d96090b93', data.response.site);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal('string', data.response.mgmt);
                assert.equal('all', data.response.icmp);
                assert.equal('string', data.response.guest);
                assert.equal(true, Array.isArray(data.response.breakout_preference));
                assert.equal(true, Array.isArray(data.response.routes));
                assert.equal(true, Array.isArray(data.response.bcasts));
                assert.equal(1000, data.response.tag);
                assert.equal('string', data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zone', 'getZoneZoneid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getZones((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zone', 'getZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchOrgid = 'fakedata';
    const switchPostOrgOrgidSwitchesBodyParam = {
      site: 'string',
      serial: 'string',
      model: 'string'
    };
    describe('#postOrgOrgidSwitches - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidSwitches(switchOrgid, switchPostOrgOrgidSwitchesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.local_as);
                assert.equal('string', data.response.router_id);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.uid);
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal(true, Array.isArray(data.response.radios));
                assert.equal('string', data.response.realm);
                assert.equal('string', data.response.location);
                assert.equal(true, Array.isArray(data.response.ports));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.inventory_version_cc);
                assert.equal(true, data.response.disable_stp);
                assert.equal('string', data.response.license);
                assert.equal('string', data.response.model);
                assert.equal(false, data.response.sitelink);
                assert.equal(true, data.response.simulated);
                assert.equal('string', data.response.ha_partner);
                assert.equal('dedicated_port', data.response.ha_control_link);
                assert.equal('string', data.response.ha_mgmt_ip);
                assert.equal('string', data.response.ha_dedicated_port);
                assert.equal(true, data.response.ha_clone_mac);
                assert.equal(true, data.response.scm_managed_vsh);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch', 'postOrgOrgidSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidSwitches - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidSwitches(switchOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch', 'getOrgOrgidSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchSwitchid = 'fakedata';
    describe('#putSwitchSwitchid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSwitchSwitchid(switchSwitchid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch', 'putSwitchSwitchid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchSwitchid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSwitchSwitchid(switchSwitchid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.local_as);
                assert.equal('string', data.response.router_id);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.uid);
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal(true, Array.isArray(data.response.radios));
                assert.equal('string', data.response.realm);
                assert.equal('string', data.response.location);
                assert.equal(true, Array.isArray(data.response.ports));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.inventory_version_cc);
                assert.equal(true, data.response.disable_stp);
                assert.equal('string', data.response.license);
                assert.equal('string', data.response.model);
                assert.equal(false, data.response.sitelink);
                assert.equal(true, data.response.simulated);
                assert.equal('string', data.response.ha_partner);
                assert.equal('dedicated_port', data.response.ha_control_link);
                assert.equal('string', data.response.ha_mgmt_ip);
                assert.equal('string', data.response.ha_dedicated_port);
                assert.equal(true, data.response.ha_clone_mac);
                assert.equal(true, data.response.scm_managed_vsh);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch', 'getSwitchSwitchid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitches - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSwitches((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch', 'getSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accesspointOrgid = 'fakedata';
    const accesspointPostOrgOrgidApBodyParam = {
      site: 'string',
      serial: 'string',
      model: 'string'
    };
    describe('#postOrgOrgidAp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidAp(accesspointOrgid, accesspointPostOrgOrgidApBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.local_as);
                assert.equal('string', data.response.router_id);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.uid);
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal(true, Array.isArray(data.response.radios));
                assert.equal('string', data.response.realm);
                assert.equal('string', data.response.location);
                assert.equal(true, Array.isArray(data.response.ports));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.inventory_version_cc);
                assert.equal(true, data.response.disable_stp);
                assert.equal('string', data.response.license);
                assert.equal('string', data.response.model);
                assert.equal(true, data.response.sitelink);
                assert.equal(true, data.response.simulated);
                assert.equal('string', data.response.ha_partner);
                assert.equal('dedicated_port', data.response.ha_control_link);
                assert.equal('string', data.response.ha_mgmt_ip);
                assert.equal('string', data.response.ha_dedicated_port);
                assert.equal(true, data.response.ha_clone_mac);
                assert.equal(true, data.response.scm_managed_vsh);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accesspoint', 'postOrgOrgidAp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAp((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accesspoint', 'getAp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accesspointApid = 'fakedata';
    describe('#putApApid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putApApid(accesspointApid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accesspoint', 'putApApid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApApid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApApid(accesspointApid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.local_as);
                assert.equal('string', data.response.router_id);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.uid);
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal(true, Array.isArray(data.response.radios));
                assert.equal('string', data.response.realm);
                assert.equal('string', data.response.location);
                assert.equal(true, Array.isArray(data.response.ports));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.inventory_version_cc);
                assert.equal(true, data.response.disable_stp);
                assert.equal('string', data.response.license);
                assert.equal('string', data.response.model);
                assert.equal(false, data.response.sitelink);
                assert.equal(true, data.response.simulated);
                assert.equal('string', data.response.ha_partner);
                assert.equal('dedicated_port', data.response.ha_control_link);
                assert.equal('string', data.response.ha_mgmt_ip);
                assert.equal('string', data.response.ha_dedicated_port);
                assert.equal(true, data.response.ha_clone_mac);
                assert.equal(true, data.response.scm_managed_vsh);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accesspoint', 'getApApid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidAp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidAp(accesspointOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accesspoint', 'getOrgOrgidAp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeNodeid = 'fakedata';
    describe('#postNodeNodeidFactoryReset - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNodeNodeidFactoryReset(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'postNodeNodeidFactoryReset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidGenerateSupportPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNodeNodeidGenerateSupportPackage(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'postNodeNodeidGenerateSupportPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidInventoryUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNodeNodeidInventoryUpdate(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'postNodeNodeidInventoryUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodePostNodeNodeidPrepareImageBodyParam = {};
    describe('#postNodeNodeidPrepareImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNodeNodeidPrepareImage(nodeNodeid, nodePostNodeNodeidPrepareImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'postNodeNodeidPrepareImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidReboot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNodeNodeidReboot(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'postNodeNodeidReboot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidRemove - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNodeNodeidRemove(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'postNodeNodeidRemove', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidRetryUpgrade - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNodeNodeidRetryUpgrade(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'postNodeNodeidRetryUpgrade', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeOrgid = 'fakedata';
    const nodePostOrgOrgidNodeRegisterBodyParam = {
      serial: 'string'
    };
    describe('#postOrgOrgidNodeRegister - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidNodeRegister(nodeOrgid, nodePostOrgOrgidNodeRegisterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.local_as);
                assert.equal('string', data.response.router_id);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.uid);
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal(true, Array.isArray(data.response.radios));
                assert.equal('string', data.response.realm);
                assert.equal('string', data.response.location);
                assert.equal(true, Array.isArray(data.response.ports));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.inventory_version_cc);
                assert.equal(true, data.response.disable_stp);
                assert.equal('string', data.response.license);
                assert.equal('string', data.response.model);
                assert.equal(true, data.response.sitelink);
                assert.equal(true, data.response.simulated);
                assert.equal('string', data.response.ha_partner);
                assert.equal('dedicated_port', data.response.ha_control_link);
                assert.equal('string', data.response.ha_mgmt_ip);
                assert.equal('string', data.response.ha_dedicated_port);
                assert.equal(true, data.response.ha_clone_mac);
                assert.equal(true, data.response.scm_managed_vsh);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'postOrgOrgidNodeRegister', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodePostOrgOrgidNodeVirtualRegisterBodyParam = {
      site: 'string',
      model: 'string'
    };
    describe('#postOrgOrgidNodeVirtualRegister - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidNodeVirtualRegister(nodeOrgid, nodePostOrgOrgidNodeVirtualRegisterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.local_as);
                assert.equal('string', data.response.router_id);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.uid);
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal(true, Array.isArray(data.response.radios));
                assert.equal('string', data.response.realm);
                assert.equal('string', data.response.location);
                assert.equal(true, Array.isArray(data.response.ports));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.inventory_version_cc);
                assert.equal(true, data.response.disable_stp);
                assert.equal('string', data.response.license);
                assert.equal('string', data.response.model);
                assert.equal(true, data.response.sitelink);
                assert.equal(true, data.response.simulated);
                assert.equal('string', data.response.ha_partner);
                assert.equal('dedicated_port', data.response.ha_control_link);
                assert.equal('string', data.response.ha_mgmt_ip);
                assert.equal('string', data.response.ha_dedicated_port);
                assert.equal(true, data.response.ha_clone_mac);
                assert.equal(false, data.response.scm_managed_vsh);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'postOrgOrgidNodeVirtualRegister', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeNodeid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putNodeNodeid(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'putNodeNodeid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNodeid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodeNodeid(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.local_as);
                assert.equal('string', data.response.router_id);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.uid);
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal(true, Array.isArray(data.response.radios));
                assert.equal('string', data.response.realm);
                assert.equal('string', data.response.location);
                assert.equal(true, Array.isArray(data.response.ports));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.inventory_version_cc);
                assert.equal(true, data.response.disable_stp);
                assert.equal('string', data.response.license);
                assert.equal('string', data.response.model);
                assert.equal(true, data.response.sitelink);
                assert.equal(true, data.response.simulated);
                assert.equal('string', data.response.ha_partner);
                assert.equal('dedicated_port', data.response.ha_control_link);
                assert.equal('string', data.response.ha_mgmt_ip);
                assert.equal('string', data.response.ha_dedicated_port);
                assert.equal(true, data.response.ha_clone_mac);
                assert.equal(true, data.response.scm_managed_vsh);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'getNodeNodeid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNodeidDownloadSupportPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNodeNodeidDownloadSupportPackage(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'getNodeNodeidDownloadSupportPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeFile = 'fakedata';
    describe('#getNodeNodeidGetImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNodeNodeidGetImage(nodeFile, nodeNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'getNodeNodeidGetImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNodeidImageStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodeNodeidImageStatus(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.error);
                assert.equal('string', data.response.image_file);
                assert.equal('string', data.response.image_type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'getNodeNodeidImageStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodes(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'getNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidNodes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidNodes(nodeOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'getOrgOrgidNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeSiteid = 'fakedata';
    describe('#getSiteSiteidNodes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteSiteidNodes(nodeSiteid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'getSiteSiteidNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const clusterOrgid = 'fakedata';
    const clusterPostOrgOrgidClustersBodyParam = {
      site: 'site-test-57e69c851c6147b4'
    };
    describe('#postOrgOrgidClusters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidClusters(clusterOrgid, clusterPostOrgOrgidClustersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('site-test-57e69c851c6147b4', data.response.site);
                assert.equal('org-example-8a882293bfddffaf', data.response.org);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.failover);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal(true, Array.isArray(data.response.dcuplinks));
                assert.equal('string', data.response.url);
                assert.equal(9, data.response.bgp_graceful_restart);
                assert.equal('string', data.response.bgp_tep_community_type);
                assert.equal(3, data.response.bgp_tep_community);
                assert.equal('string', data.response.bgp_branch_community_type);
                assert.equal('string', data.response.bgp_branch_community);
                assert.equal(4, data.response.bgp_deployment_mode);
                assert.equal(6, data.response.bgp_subnet_splitting);
                assert.equal(1, data.response.bgp_interceptor);
                assert.equal('string', data.response.proxy_ip);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cluster', 'postOrgOrgidClusters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const clusterClusterid = 'fakedata';
    const clusterPutClusterClusteridBodyParam = {
      site: 'site-test-57e69c851c6147b4'
    };
    describe('#putClusterClusterid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putClusterClusterid(clusterClusterid, clusterPutClusterClusteridBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cluster', 'putClusterClusterid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterClusterid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getClusterClusterid(clusterClusterid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('site-test-57e69c851c6147b4', data.response.site);
                assert.equal('org-example-8a882293bfddffaf', data.response.org);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.failover);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal(true, Array.isArray(data.response.dcuplinks));
                assert.equal('string', data.response.url);
                assert.equal(3, data.response.bgp_graceful_restart);
                assert.equal('string', data.response.bgp_tep_community_type);
                assert.equal(10, data.response.bgp_tep_community);
                assert.equal('string', data.response.bgp_branch_community_type);
                assert.equal('string', data.response.bgp_branch_community);
                assert.equal(10, data.response.bgp_deployment_mode);
                assert.equal(2, data.response.bgp_subnet_splitting);
                assert.equal(2, data.response.bgp_interceptor);
                assert.equal('string', data.response.proxy_ip);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cluster', 'getClusterClusterid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getClusters((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cluster', 'getClusters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidClusters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidClusters(clusterOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cluster', 'getOrgOrgidClusters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const clusterSiteid = 'fakedata';
    describe('#getSiteSiteidClusters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteSiteidClusters(clusterSiteid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cluster', 'getSiteSiteidClusters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcuplinkOrgid = 'fakedata';
    const dcuplinkPostOrgOrgidDcuplinksBodyParam = {
      cluster: 'string'
    };
    describe('#postOrgOrgidDcuplinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidDcuplinks(dcuplinkOrgid, dcuplinkPostOrgOrgidDcuplinksBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.cluster);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.net);
                assert.equal('string', data.response.public_ipv4);
                assert.equal('string', data.response.public_ipv6);
                assert.equal('string', data.response.wan);
                assert.equal('string', data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcuplink', 'postOrgOrgidDcuplinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcuplinkDcuplinkid = 'fakedata';
    const dcuplinkPutDcuplinkDcuplinkidBodyParam = {
      cluster: 'string'
    };
    describe('#putDcuplinkDcuplinkid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcuplinkDcuplinkid(dcuplinkDcuplinkid, dcuplinkPutDcuplinkDcuplinkidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcuplink', 'putDcuplinkDcuplinkid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcuplinkDcuplinkid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcuplinkDcuplinkid(dcuplinkDcuplinkid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.cluster);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.net);
                assert.equal('string', data.response.public_ipv4);
                assert.equal('string', data.response.public_ipv6);
                assert.equal('string', data.response.wan);
                assert.equal('string', data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcuplink', 'getDcuplinkDcuplinkid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcuplinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcuplinks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcuplink', 'getDcuplinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidDcuplinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidDcuplinks(dcuplinkOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcuplink', 'getOrgOrgidDcuplinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ssidOrgid = 'fakedata';
    const ssidPostOrgOrgidSsidsBodyParam = {
      ssid: 'admin',
      security: 'wpa2personal',
      key: 'string'
    };
    describe('#postOrgOrgidSsids - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidSsids(ssidOrgid, ssidPostOrgOrgidSsidsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('admin', data.response.ssid);
                assert.equal('wpa2personal', data.response.security);
                assert.equal('string', data.response.encryption);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.authentication);
                assert.equal('string', data.response.eapol_version);
                assert.equal('string', data.response.dtim_period);
                assert.equal(true, Array.isArray(data.response.bcasts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssid', 'postOrgOrgidSsids', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidSsids - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidSsids(ssidOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('admin', data.response.ssid);
                assert.equal('wpa2personal', data.response.security);
                assert.equal('string', data.response.encryption);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.authentication);
                assert.equal('string', data.response.eapol_version);
                assert.equal('string', data.response.dtim_period);
                assert.equal(true, Array.isArray(data.response.bcasts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssid', 'getOrgOrgidSsids', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ssidSsidid = 'fakedata';
    describe('#putSsidSsidid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSsidSsidid(ssidSsidid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssid', 'putSsidSsidid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSsidSsidid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSsidSsidid(ssidSsidid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('admin', data.response.ssid);
                assert.equal('wpa2personal', data.response.security);
                assert.equal('string', data.response.encryption);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.authentication);
                assert.equal('string', data.response.eapol_version);
                assert.equal('string', data.response.dtim_period);
                assert.equal(true, Array.isArray(data.response.bcasts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssid', 'getSsidSsidid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSsids - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSsids((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssid', 'getSsids', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const broadcastOrgid = 'fakedata';
    const broadcastPostOrgOrgidBroadcastsBodyParam = {
      site: 'string',
      zone: 'string',
      ssid: 'string'
    };
    describe('#postOrgOrgidBroadcasts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidBroadcasts(broadcastOrgid, broadcastPostOrgOrgidBroadcastsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.zone);
                assert.equal('string', data.response.ssid);
                assert.equal(true, data.response.inactive);
                assert.equal(true, data.response.dynzone);
                assert.equal('string', data.response.portal);
                assert.equal(false, data.response.hide_ssid);
                assert.equal('string', data.response.band);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Broadcast', 'postOrgOrgidBroadcasts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const broadcastBcastid = 'fakedata';
    const broadcastPutBroadcastBcastidBodyParam = {
      site: 'string',
      zone: 'string',
      ssid: 'string'
    };
    describe('#putBroadcastBcastid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putBroadcastBcastid(broadcastBcastid, broadcastPutBroadcastBcastidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Broadcast', 'putBroadcastBcastid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBroadcastBcastid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBroadcastBcastid(broadcastBcastid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.zone);
                assert.equal('string', data.response.ssid);
                assert.equal(false, data.response.inactive);
                assert.equal(true, data.response.dynzone);
                assert.equal('string', data.response.portal);
                assert.equal(true, data.response.hide_ssid);
                assert.equal('string', data.response.band);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Broadcast', 'getBroadcastBcastid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBroadcasts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBroadcasts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Broadcast', 'getBroadcasts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidBroadcasts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidBroadcasts(broadcastOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Broadcast', 'getOrgOrgidBroadcasts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const broadcastSiteid = 'fakedata';
    describe('#getSiteSiteidBroadcasts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteSiteidBroadcasts(broadcastSiteid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Broadcast', 'getSiteSiteidBroadcasts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let appAppid = 'fakedata';
    const appOrgid = 'fakedata';
    const appPostOrgOrgidCustomAppsBodyParam = {
      name: 'string'
    };
    describe('#postOrgOrgidCustomApps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidCustomApps(appOrgid, appPostOrgOrgidCustomAppsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.appid);
                assert.equal('string', data.response.desc);
                assert.equal(true, Array.isArray(data.response.appgrps));
                assert.equal('string', data.response.devgrp);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.dnats));
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.internal);
                assert.equal('string', data.response.ipport);
                assert.equal('\'facebook.*\'\n \'cnn.com\'\n \'ibm.*\'', data.response.httphost);
                assert.equal('string', data.response.device);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal('string', data.response.device_proto);
                assert.equal('string', data.response.device_ports);
                assert.equal('string', data.response.uid);
              } else {
                runCommonAsserts(data, error);
              }
              appAppid = data.response.appid;
              saveMockData('App', 'postOrgOrgidCustomApps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppAppid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppAppid(appAppid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.desc);
                assert.equal('string', data.response.dgrp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppAppid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApps((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getApps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCustomAppAppid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCustomAppAppid(appAppid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'putCustomAppAppid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomAppAppid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomAppAppid(appAppid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.appid);
                assert.equal('string', data.response.desc);
                assert.equal(true, Array.isArray(data.response.appgrps));
                assert.equal('string', data.response.devgrp);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.dnats));
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.internal);
                assert.equal('string', data.response.ipport);
                assert.equal('\'facebook.*\'\n \'cnn.com\'\n \'ibm.*\'', data.response.httphost);
                assert.equal('string', data.response.device);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal('string', data.response.device_proto);
                assert.equal('string', data.response.device_ports);
                assert.equal('string', data.response.uid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getCustomAppAppid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomApps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomApps((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getCustomApps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCustomApps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidCustomApps(appOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getOrgOrgidCustomApps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appgrpOrgid = 'fakedata';
    const appgrpPostOrgOrgidAppGroupsBodyParam = {
      name: 'string'
    };
    describe('#postOrgOrgidAppGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidAppGroups(appgrpOrgid, appgrpPostOrgOrgidAppGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.webcat);
                assert.equal('string', data.response.sapps);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.predefined);
                assert.equal(true, Array.isArray(data.response.apps));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.desc);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appgrp', 'postOrgOrgidAppGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appgrpAppgrpid = 'fakedata';
    describe('#putAppGroupAppgrpid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAppGroupAppgrpid(appgrpAppgrpid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appgrp', 'putAppGroupAppgrpid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppGroupAppgrpid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppGroupAppgrpid(appgrpAppgrpid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.webcat);
                assert.equal('string', data.response.sapps);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.predefined);
                assert.equal(true, Array.isArray(data.response.apps));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.desc);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appgrp', 'getAppGroupAppgrpid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appgrp', 'getAppGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidAppGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidAppGroups(appgrpOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appgrp', 'getOrgOrgidAppGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userOrgid = 'fakedata';
    const userPostOrgOrgidUsersBodyParam = {
      name: 'string',
      email: 'string'
    };
    describe('#postOrgOrgidUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidUsers(userOrgid, userPostOrgOrgidUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.uid);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.usergrps));
                assert.equal('string', data.response.home_site);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.mobile);
                assert.equal(true, Array.isArray(data.response.endpoints));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'postOrgOrgidUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidUsers(userOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getOrgOrgidUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userUserid = 'fakedata';
    describe('#putUserUserid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putUserUserid(userUserid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'putUserUserid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserUserid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserUserid(userUserid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.uid);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.usergrps));
                assert.equal('string', data.response.home_site);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.mobile);
                assert.equal(true, Array.isArray(data.response.endpoints));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUserUserid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceOrgid = 'fakedata';
    const devicePostOrgOrgidDevicesBodyParam = {
      mac: 'string'
    };
    describe('#postOrgOrgidDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidDevices(deviceOrgid, devicePostOrgOrgidDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.uid);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.mac);
                assert.equal('string', data.response.info);
                assert.equal('string', data.response.ipv4);
                assert.equal('string', data.response.ipv6);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.devgrps));
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.net);
                assert.equal('string', data.response.endpoint);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'postOrgOrgidDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceDevid = 'fakedata';
    describe('#putDeviceDevid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDeviceDevid(deviceDevid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'putDeviceDevid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDevid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDevid(deviceDevid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.uid);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.mac);
                assert.equal('string', data.response.info);
                assert.equal('string', data.response.ipv4);
                assert.equal('string', data.response.ipv6);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.devgrps));
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.net);
                assert.equal('string', data.response.endpoint);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceDevid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidDevices(deviceOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getOrgOrgidDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pathruleOrgid = 'fakedata';
    const pathrulePostOrgOrgidPathRulesBodyParam = {
      name: 'string'
    };
    describe('#postOrgOrgidPathRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidPathRules(pathruleOrgid, pathrulePostOrgOrgidPathRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.dsttype);
                assert.equal('string', data.response.srctype);
                assert.equal('string', data.response.qos);
                assert.equal('dscp_0', data.response.marking);
                assert.equal(true, Array.isArray(data.response.source_zones));
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal('string', data.response.uid);
                assert.equal(true, data.response.active);
                assert.equal(true, Array.isArray(data.response.sites));
                assert.equal(true, Array.isArray(data.response.path_preference));
                assert.equal(true, data.response.path_pq_drop);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.dscp);
                assert.equal(true, Array.isArray(data.response.apps));
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal('string', data.response.tags);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal('string', data.response.wan_opt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pathrule', 'postOrgOrgidPathRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidPathRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidPathRules(pathruleOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pathrule', 'getOrgOrgidPathRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pathrulePruleid = 'fakedata';
    describe('#putPathRulePruleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putPathRulePruleid(pathrulePruleid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pathrule', 'putPathRulePruleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathRulePruleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPathRulePruleid(pathrulePruleid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.dsttype);
                assert.equal('string', data.response.srctype);
                assert.equal('string', data.response.qos);
                assert.equal('dscp_0', data.response.marking);
                assert.equal(true, Array.isArray(data.response.source_zones));
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal('string', data.response.uid);
                assert.equal(true, data.response.active);
                assert.equal(true, Array.isArray(data.response.sites));
                assert.equal(true, Array.isArray(data.response.path_preference));
                assert.equal(false, data.response.path_pq_drop);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.dscp);
                assert.equal(true, Array.isArray(data.response.apps));
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal('string', data.response.tags);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal('string', data.response.wan_opt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pathrule', 'getPathRulePruleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPathRules((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pathrule', 'getPathRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const outboundruleOrgid = 'fakedata';
    const outboundrulePostOrgOrgidOutboundRulesBodyParam = {
      name: 'string'
    };
    describe('#postOrgOrgidOutboundRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidOutboundRules(outboundruleOrgid, outboundrulePostOrgOrgidOutboundRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(true, data.response.active);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal(true, Array.isArray(data.response.apps));
                assert.equal('string', data.response.sapps);
                assert.equal('all', data.response.srctype);
                assert.equal(true, Array.isArray(data.response.source_zones));
                assert.equal('string', data.response.dsttype);
                assert.equal('string', data.response.tags);
                assert.equal(true, data.response.allow);
                assert.equal(true, Array.isArray(data.response.usergrps));
                assert.equal(true, Array.isArray(data.response.devgrps));
                assert.equal(true, Array.isArray(data.response.appgrps));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outboundrule', 'postOrgOrgidOutboundRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidOutboundRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidOutboundRules(outboundruleOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outboundrule', 'getOrgOrgidOutboundRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const outboundruleRuleid = 'fakedata';
    describe('#putOutboundRuleRuleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putOutboundRuleRuleid(outboundruleRuleid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outboundrule', 'putOutboundRuleRuleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOutboundRuleRuleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOutboundRuleRuleid(outboundruleRuleid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(true, data.response.active);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal(true, Array.isArray(data.response.zones));
                assert.equal(true, Array.isArray(data.response.apps));
                assert.equal('string', data.response.sapps);
                assert.equal('all', data.response.srctype);
                assert.equal(true, Array.isArray(data.response.source_zones));
                assert.equal('string', data.response.dsttype);
                assert.equal('string', data.response.tags);
                assert.equal(true, data.response.allow);
                assert.equal(true, Array.isArray(data.response.usergrps));
                assert.equal(true, Array.isArray(data.response.devgrps));
                assert.equal(true, Array.isArray(data.response.appgrps));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outboundrule', 'getOutboundRuleRuleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOutboundRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOutboundRules((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outboundrule', 'getOutboundRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inboundruleOrgid = 'fakedata';
    const inboundrulePostOrgOrgidInboundRulesBodyParam = {
      app: 'string',
      name: 'string'
    };
    describe('#postOrgOrgidInboundRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidInboundRules(inboundruleOrgid, inboundrulePostOrgOrgidInboundRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.nat_port_offset);
                assert.equal('string', data.response.app);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.no_reflection);
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.mode);
                assert.equal('string', data.response.id);
                assert.equal(false, data.response.inactive);
                assert.equal('string', data.response.custom_ip);
                assert.equal('string', data.response.hostlist);
                assert.equal(false, data.response.use_hostlist);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inboundrule', 'postOrgOrgidInboundRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inboundruleRuleid = 'fakedata';
    describe('#putInboundRuleRuleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putInboundRuleRuleid(inboundruleRuleid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inboundrule', 'putInboundRuleRuleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInboundRuleRuleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInboundRuleRuleid(inboundruleRuleid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.nat_port_offset);
                assert.equal('string', data.response.app);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.no_reflection);
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.mode);
                assert.equal('string', data.response.id);
                assert.equal(false, data.response.inactive);
                assert.equal('string', data.response.custom_ip);
                assert.equal('string', data.response.hostlist);
                assert.equal(true, data.response.use_hostlist);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inboundrule', 'getInboundRuleRuleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInboundRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInboundRules((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inboundrule', 'getInboundRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidInboundRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidInboundRules(inboundruleOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inboundrule', 'getOrgOrgidInboundRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const endpointOrgid = 'fakedata';
    const endpointPostOrgOrgidEndpointsBodyParam = {
      user: 'string',
      client_id: 'string'
    };
    describe('#postOrgOrgidEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidEndpoints(endpointOrgid, endpointPostOrgOrgidEndpointsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.vmac);
                assert.equal('string', data.response.secret);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.client_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Endpoint', 'postOrgOrgidEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const endpointEpid = 'fakedata';
    describe('#putEndpointEpid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putEndpointEpid(endpointEpid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Endpoint', 'putEndpointEpid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndpointEpid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEndpointEpid(endpointEpid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.vmac);
                assert.equal('string', data.response.secret);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.client_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Endpoint', 'getEndpointEpid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEndpoints((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Endpoint', 'getEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidEndpoints(endpointOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Endpoint', 'getOrgOrgidEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkOrgid = 'fakedata';
    const networkPostOrgOrgidNetworksBodyParam = {
      name: 'string',
      zone: 'string',
      site: 'string'
    };
    describe('#postOrgOrgidNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidNetworks(networkOrgid, networkPostOrgOrgidNetworksBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.zone);
                assert.equal('string', data.response.site);
                assert.equal(true, Array.isArray(data.response.nodenetcfgs));
                assert.equal('string', data.response.dhcps_range_start);
                assert.equal('string', data.response.dhcps_range_end);
                assert.equal(4, data.response.dhcps_leasetime);
                assert.equal('string', data.response.dhcps_options);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal('string', data.response.primary);
                assert.equal('string', data.response.netv6);
                assert.equal('string', data.response.gwv6);
                assert.equal('string', data.response.netv4);
                assert.equal('string', data.response.gwv4);
                assert.equal('string', data.response.org);
                assert.equal(false, data.response.ra);
                assert.equal(true, Array.isArray(data.response.wans));
                assert.equal(true, Array.isArray(data.response.routes));
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.nets));
                assert.equal(true, Array.isArray(data.response.breakout_preference));
                assert.equal('string', data.response.breakout_sitelink_site);
                assert.equal(false, data.response.gw_noauto);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'postOrgOrgidNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkNetid = 'fakedata';
    describe('#putNetworkNetid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putNetworkNetid(networkNetid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'putNetworkNetid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkNetid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkNetid(networkNetid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.zone);
                assert.equal('string', data.response.site);
                assert.equal(true, Array.isArray(data.response.nodenetcfgs));
                assert.equal('string', data.response.dhcps_range_start);
                assert.equal('string', data.response.dhcps_range_end);
                assert.equal(10, data.response.dhcps_leasetime);
                assert.equal('string', data.response.dhcps_options);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal('string', data.response.primary);
                assert.equal('string', data.response.netv6);
                assert.equal('string', data.response.gwv6);
                assert.equal('string', data.response.netv4);
                assert.equal('string', data.response.gwv4);
                assert.equal('string', data.response.org);
                assert.equal(true, data.response.ra);
                assert.equal(true, Array.isArray(data.response.wans));
                assert.equal(true, Array.isArray(data.response.routes));
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.nets));
                assert.equal(true, Array.isArray(data.response.breakout_preference));
                assert.equal('string', data.response.breakout_sitelink_site);
                assert.equal(false, data.response.gw_noauto);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'getNetworkNetid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'getNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidNetworks(networkOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'getOrgOrgidNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portNodeid = 'fakedata';
    describe('#getNodeNodeidPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodeNodeidPorts(portNodeid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.port_id);
                assert.equal('string', data.response.node);
                assert.equal('string', data.response.tag);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.speeds);
                assert.equal('string', data.response.speed);
                assert.equal('string', data.response.patchlabel);
                assert.equal('string', data.response.zone);
                assert.equal('string', data.response.uplink);
                assert.equal('string', data.response.portal);
                assert.equal('string', data.response.mac);
                assert.equal('string', data.response.virtual_mac);
                assert.equal('string', data.response.switch_id);
                assert.equal(true, data.response.autotrunk);
                assert.equal('string', data.response.bridge_with);
                assert.equal('string', data.response.ifname);
                assert.equal('string', data.response.dcinterface);
                assert.equal(false, data.response.auto);
                assert.equal(false, data.response.autocfg);
                assert.equal(false, data.response.tagged);
                assert.equal('string', data.response.segment);
                assert.equal(true, data.response.vsh_aux_enabled);
                assert.equal(false, data.response.vsh_primary_enabled);
                assert.equal(false, data.response.cluster_enabled);
                assert.equal(true, data.response.data_enabled);
                assert.equal(true, data.response.mgmt_enabled);
                assert.equal('string', data.response.mgmt_config_type);
                assert.equal('string', data.response.mgmt_ipv4);
                assert.equal('string', data.response.mgmt_gw_ipv4);
                assert.equal('string', data.response.mgmt_ipv6);
                assert.equal('string', data.response.mgmt_gw_ipv6);
                assert.equal('string', data.response.mgmt_dns_ipv4);
                assert.equal(true, data.response.poe_disabled);
                assert.equal(false, data.response.capab_multizone);
                assert.equal(false, data.response.capab_vsh_aux);
                assert.equal(false, data.response.capab_mgmt);
                assert.equal(false, data.response.capab_lan);
                assert.equal(false, data.response.capab_poe);
                assert.equal(true, data.response.capab_uplink);
                assert.equal(true, data.response.capab_cluster);
                assert.equal(false, data.response.ha_mirrored_uplink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Port', 'getNodeNodeidPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portPortid = 'fakedata';
    describe('#putPortPortid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putPortPortid(portPortid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Port', 'putPortPortid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortPortid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortPortid(portPortid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.port_id);
                assert.equal('string', data.response.node);
                assert.equal('string', data.response.tag);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.speeds);
                assert.equal('string', data.response.speed);
                assert.equal('string', data.response.patchlabel);
                assert.equal('string', data.response.zone);
                assert.equal('string', data.response.uplink);
                assert.equal('string', data.response.portal);
                assert.equal('string', data.response.mac);
                assert.equal('string', data.response.virtual_mac);
                assert.equal('string', data.response.switch_id);
                assert.equal(true, data.response.autotrunk);
                assert.equal('string', data.response.bridge_with);
                assert.equal('string', data.response.ifname);
                assert.equal('string', data.response.dcinterface);
                assert.equal(true, data.response.auto);
                assert.equal(true, data.response.autocfg);
                assert.equal(false, data.response.tagged);
                assert.equal('string', data.response.segment);
                assert.equal(false, data.response.vsh_aux_enabled);
                assert.equal(false, data.response.vsh_primary_enabled);
                assert.equal(false, data.response.cluster_enabled);
                assert.equal(false, data.response.data_enabled);
                assert.equal(false, data.response.mgmt_enabled);
                assert.equal('string', data.response.mgmt_config_type);
                assert.equal('string', data.response.mgmt_ipv4);
                assert.equal('string', data.response.mgmt_gw_ipv4);
                assert.equal('string', data.response.mgmt_ipv6);
                assert.equal('string', data.response.mgmt_gw_ipv6);
                assert.equal('string', data.response.mgmt_dns_ipv4);
                assert.equal(true, data.response.poe_disabled);
                assert.equal(true, data.response.capab_multizone);
                assert.equal(false, data.response.capab_vsh_aux);
                assert.equal(true, data.response.capab_mgmt);
                assert.equal(false, data.response.capab_lan);
                assert.equal(true, data.response.capab_poe);
                assert.equal(false, data.response.capab_uplink);
                assert.equal(true, data.response.capab_cluster);
                assert.equal(false, data.response.ha_mirrored_uplink);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Port', 'getPortPortid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPorts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Port', 'getPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wanOrgid = 'fakedata';
    const wanPostOrgOrgidWansBodyParam = {
      id: 'string',
      org: 'string',
      uplinks: [
        'string'
      ],
      nets: [
        'string'
      ],
      name: 'string',
      longname: 'string',
      uid: 'string',
      internet: true,
      sitelink: false,
      pingcheck_ips: 'string',
      dcuplink: [
        'string'
      ],
      breakout: false,
      breakout_sites: [
        'string'
      ],
      xfer_networks: 'string'
    };
    describe('#postOrgOrgidWans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidWans(wanOrgid, wanPostOrgOrgidWansBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal(true, Array.isArray(data.response.nets));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.longname);
                assert.equal('string', data.response.uid);
                assert.equal(true, data.response.internet);
                assert.equal(false, data.response.sitelink);
                assert.equal('string', data.response.pingcheck_ips);
                assert.equal(true, Array.isArray(data.response.dcuplink));
                assert.equal(true, data.response.breakout);
                assert.equal(true, Array.isArray(data.response.breakout_sites));
                assert.equal('string', data.response.xfer_networks);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wan', 'postOrgOrgidWans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidWans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidWans(wanOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wan', 'getOrgOrgidWans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wanWanid = 'fakedata';
    describe('#putWanWanid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWanWanid(wanWanid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wan', 'putWanWanid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanWanid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWanWanid(wanWanid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal(true, Array.isArray(data.response.nets));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.longname);
                assert.equal('string', data.response.uid);
                assert.equal(false, data.response.internet);
                assert.equal(false, data.response.sitelink);
                assert.equal('string', data.response.pingcheck_ips);
                assert.equal(true, Array.isArray(data.response.dcuplink));
                assert.equal(true, data.response.breakout);
                assert.equal(true, Array.isArray(data.response.breakout_sites));
                assert.equal('string', data.response.xfer_networks);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wan', 'getWanWanid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWans((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wan', 'getWans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshtunnelNodeid = 'fakedata';
    describe('#postSshtunnelNodeid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSshtunnelNodeid(sshtunnelNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshtunnel', 'postSshtunnelNodeid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSshtunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSshtunnel((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshtunnel', 'getSshtunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSshtunnelNodeid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSshtunnelNodeid(sshtunnelNodeid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('node-254de7b540fa3d3b', data.response.node_id);
                assert.equal('connected', data.response.status);
                assert.equal(1488984973, data.response.ssh_launch);
                assert.equal(1488984973, data.response.ssh_connect);
                assert.equal('scm.example.cc', data.response.hostname);
                assert.equal(4053, data.response.proxyport);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.ssh_help);
                assert.equal('string', data.response.http_help);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshtunnel', 'getSshtunnelNodeid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const proxyserviceOrgid = 'fakedata';
    describe('#postOrgOrgidProxyserviceSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidProxyserviceSettings(proxyserviceOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('zscalerone', data.response.cloud);
                assert.equal('asdgasdgasdfa', data.response.psk);
                assert.equal(true, Array.isArray(data.response.site_settings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'postOrgOrgidProxyserviceSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidProxyservices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidProxyservices(proxyserviceOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('org-mytestorg-3ead355de85c0149', data.response.org);
                assert.equal('zscaler', data.response.type);
                assert.equal('123.23.1.14', data.response.tunnelip);
                assert.equal(true, data.response.enabled);
                assert.equal(8, data.response.lastmodified);
                assert.equal('string', data.response.city);
                assert.equal('string', data.response.country);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'postOrgOrgidProxyservices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postProxyserviceCommand - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postProxyserviceCommand((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'postProxyserviceCommand', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidProxyserviceConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidProxyserviceConfig(proxyserviceOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Action,PSK Type,VPN User Name,Comments,Pre-Shared Key, +,UFQDN,john.doe@john.com,imported FDQN user,some_psk_value,\n', data.response.config);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'getOrgOrgidProxyserviceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidProxyserviceSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidProxyserviceSettings(proxyserviceOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('zscalerone', data.response.cloud);
                assert.equal('asdgasdgasdfa', data.response.psk);
                assert.equal(true, Array.isArray(data.response.site_settings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'getOrgOrgidProxyserviceSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidProxyservices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidProxyservices(proxyserviceOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'getOrgOrgidProxyservices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const proxyserviceProxyserviceid = 'fakedata';
    describe('#putProxyserviceProxyserviceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putProxyserviceProxyserviceid(proxyserviceProxyserviceid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'putProxyserviceProxyserviceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProxyserviceProxyserviceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProxyserviceProxyserviceid(proxyserviceProxyserviceid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('org-mytestorg-3ead355de85c0149', data.response.org);
                assert.equal('zscaler', data.response.type);
                assert.equal('123.23.1.14', data.response.tunnelip);
                assert.equal(true, data.response.enabled);
                assert.equal(7, data.response.lastmodified);
                assert.equal('string', data.response.city);
                assert.equal('string', data.response.country);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'getProxyserviceProxyserviceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProxyservices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProxyservices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'getProxyservices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usergrpOrgid = 'fakedata';
    const usergrpPostOrgOrgidUsergrpsBodyParam = {
      org: 'org-acme-0c5027cd98540e76',
      name: 'string'
    };
    describe('#postOrgOrgidUsergrps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidUsergrps(usergrpOrgid, usergrpPostOrgOrgidUsergrpsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('org-acme-0c5027cd98540e76', data.response.org);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.dir_type);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.desc);
                assert.equal('string', data.response.dir_sync_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Usergrp', 'postOrgOrgidUsergrps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidUsergrps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidUsergrps(usergrpOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Usergrp', 'getOrgOrgidUsergrps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usergrpUsergrpid = 'fakedata';
    describe('#putUsergrpUsergrpid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putUsergrpUsergrpid(usergrpUsergrpid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Usergrp', 'putUsergrpUsergrpid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsergrpUsergrpid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsergrpUsergrpid(usergrpUsergrpid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('org-acme-0c5027cd98540e76', data.response.org);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.dir_type);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal('string', data.response.tags);
                assert.equal('string', data.response.desc);
                assert.equal('string', data.response.dir_sync_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Usergrp', 'getUsergrpUsergrpid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsergrps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsergrps((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Usergrp', 'getUsergrps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portalOrgid = 'fakedata';
    const portalPostOrgOrgidPortalsBodyParam = {
      name: 'ExamplePortal'
    };
    describe('#postOrgOrgidPortals - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidPortals(portalOrgid, portalPostOrgOrgidPortalsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('ExamplePortal', data.response.name);
                assert.equal('string', data.response.inactivity_timeout);
                assert.equal('string', data.response.mobile);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.social);
                assert.equal('string', data.response.tos);
                assert.equal('string', data.response.tos_show);
                assert.equal('string', data.response.voucher_tmpl);
                assert.equal('string', data.response.text_greeting);
                assert.equal('string', data.response.voucher_msg_tmpl);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.voucher_pass);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Portal', 'postOrgOrgidPortals', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portalPortalid = 'fakedata';
    describe('#postPortalPortalidPortalLogo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPortalPortalidPortalLogo(portalPortalid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Portal', 'postPortalPortalidPortalLogo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidPortals - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidPortals(portalOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Portal', 'getOrgOrgidPortals', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortal((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Portal', 'getPortal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPortalPortalid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putPortalPortalid(portalPortalid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Portal', 'putPortalPortalid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortalPortalid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortalPortalid(portalPortalid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('ExamplePortal', data.response.name);
                assert.equal('string', data.response.inactivity_timeout);
                assert.equal('string', data.response.mobile);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.social);
                assert.equal('string', data.response.tos);
                assert.equal('string', data.response.tos_show);
                assert.equal('string', data.response.voucher_tmpl);
                assert.equal('string', data.response.text_greeting);
                assert.equal('string', data.response.voucher_msg_tmpl);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.voucher_pass);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Portal', 'getPortalPortalid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ospfnetOrgid = 'fakedata';
    const ospfnetPostOrgOrgidOspfnetsBodyParam = {
      name: 'SiteName-ospf',
      site: 'string'
    };
    describe('#postOrgOrgidOspfnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidOspfnets(ospfnetOrgid, ospfnetPostOrgOrgidOspfnetsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SiteName-ospf', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.site);
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal(true, Array.isArray(data.response.areas));
                assert.equal('string', data.response.password);
                assert.equal(1, data.response.cost);
                assert.equal(false, data.response.auth_method);
                assert.equal(1, data.response.hello_interval);
                assert.equal(9, data.response.dead_interval);
                assert.equal(false, data.response.inherit);
                assert.equal(5, data.response.md5_key_id);
                assert.equal(9, data.response.priority);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospfnet', 'postOrgOrgidOspfnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidOspfnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidOspfnets(ospfnetOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospfnet', 'getOrgOrgidOspfnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ospfnetOspfnetid = 'fakedata';
    describe('#putOspfnetOspfnetid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putOspfnetOspfnetid(ospfnetOspfnetid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospfnet', 'putOspfnetOspfnetid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOspfnetOspfnetid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOspfnetOspfnetid(ospfnetOspfnetid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SiteName-ospf', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.site);
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.org);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal(true, Array.isArray(data.response.areas));
                assert.equal('string', data.response.password);
                assert.equal(2, data.response.cost);
                assert.equal(false, data.response.auth_method);
                assert.equal(3, data.response.hello_interval);
                assert.equal(5, data.response.dead_interval);
                assert.equal(false, data.response.inherit);
                assert.equal(2, data.response.md5_key_id);
                assert.equal(7, data.response.priority);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospfnet', 'getOspfnetOspfnetid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOspfnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOspfnets((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospfnet', 'getOspfnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rollingUpgradeOrgid = 'fakedata';
    const rollingUpgradePostOrgOrgidRollingUpgradesBodyParam = {
      org: 'org-acme-0c5027cd98540e76',
      rolling_time: 'string'
    };
    describe('#postOrgOrgidRollingUpgrades - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidRollingUpgrades(rollingUpgradeOrgid, rollingUpgradePostOrgOrgidRollingUpgradesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('org-acme-0c5027cd98540e76', data.response.org);
                assert.equal('string', data.response.rolling_time);
                assert.equal('string', data.response.id);
                assert.equal(1, data.response.rolling_freq);
                assert.equal(false, data.response.rolling_localtz);
                assert.equal('string', data.response.site_tag);
                assert.equal(6, data.response.rolling_wday);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RollingUpgrade', 'postOrgOrgidRollingUpgrades', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidRollingUpgrades - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidRollingUpgrades(rollingUpgradeOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RollingUpgrade', 'getOrgOrgidRollingUpgrades', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rollingUpgradeScheduleid = 'fakedata';
    describe('#putRollingUpgradeScheduleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRollingUpgradeScheduleid(rollingUpgradeScheduleid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RollingUpgrade', 'putRollingUpgradeScheduleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRollingUpgradeScheduleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRollingUpgradeScheduleid(rollingUpgradeScheduleid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('org-acme-0c5027cd98540e76', data.response.org);
                assert.equal('string', data.response.rolling_time);
                assert.equal('string', data.response.id);
                assert.equal(1, data.response.rolling_freq);
                assert.equal(false, data.response.rolling_localtz);
                assert.equal('string', data.response.site_tag);
                assert.equal(2, data.response.rolling_wday);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RollingUpgrade', 'getRollingUpgradeScheduleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRollingUpgrades - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRollingUpgrades((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RollingUpgrade', 'getRollingUpgrades', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wifiPlanOrgid = 'fakedata';
    const wifiPlanPostOrgOrgidWifiPlansBodyParam = {
      name: 'SiteName-wifi-plan',
      org: 'string',
      site: 'string'
    };
    describe('#postOrgOrgidWifiPlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidWifiPlans(wifiPlanOrgid, wifiPlanPostOrgOrgidWifiPlansBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SiteName-wifi-plan', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.profile);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.site);
                assert.equal(7, data.response.meter_xh);
                assert.equal(8, data.response.meter_w);
                assert.equal(7, data.response.image_height);
                assert.equal(1, data.response.image_zoom_scale);
                assert.equal('string', data.response.image_translation);
                assert.equal('string', data.response.image_ext);
                assert.equal(2, data.response.meter_h);
                assert.equal(9, data.response.meter_y);
                assert.equal(6, data.response.meter_xw);
                assert.equal('string', data.response.location);
                assert.equal(10, data.response.meter_x);
                assert.equal(9, data.response.image_width);
                assert.equal(5, data.response.scale);
                assert.equal('string', data.response.nodes);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WifiPlan', 'postOrgOrgidWifiPlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidWifiPlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidWifiPlans(wifiPlanOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WifiPlan', 'getOrgOrgidWifiPlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wifiPlanWifiPlanid = 'fakedata';
    describe('#putWifiPlanWifiPlanid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWifiPlanWifiPlanid(wifiPlanWifiPlanid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WifiPlan', 'putWifiPlanWifiPlanid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWifiPlanWifiPlanid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWifiPlanWifiPlanid(wifiPlanWifiPlanid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SiteName-wifi-plan', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.profile);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.site);
                assert.equal(3, data.response.meter_xh);
                assert.equal(4, data.response.meter_w);
                assert.equal(5, data.response.image_height);
                assert.equal(10, data.response.image_zoom_scale);
                assert.equal('string', data.response.image_translation);
                assert.equal('string', data.response.image_ext);
                assert.equal(3, data.response.meter_h);
                assert.equal(3, data.response.meter_y);
                assert.equal(4, data.response.meter_xw);
                assert.equal('string', data.response.location);
                assert.equal(7, data.response.meter_x);
                assert.equal(5, data.response.image_width);
                assert.equal(3, data.response.scale);
                assert.equal('string', data.response.nodes);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WifiPlan', 'getWifiPlanWifiPlanid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWifiPlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWifiPlans((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WifiPlan', 'getWifiPlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cvpnOrgid = 'fakedata';
    const cvpnPostOrgOrgidCvpnsBodyParam = {
      org: 'string',
      name: 'SiteName-cvpn',
      rgw: 'string'
    };
    describe('#postOrgOrgidCvpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidCvpns(cvpnOrgid, cvpnPostOrgOrgidCvpnsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.uplink);
                assert.equal(true, Array.isArray(data.response.nets));
                assert.equal('SiteName-cvpn', data.response.name);
                assert.equal('string', data.response.uid);
                assert.equal('string', data.response.dev);
                assert.equal('string', data.response.rgw);
                assert.equal('string', data.response.auth);
                assert.equal('string', data.response.psk);
                assert.equal('string', data.response.x509_local_type);
                assert.equal('string', data.response.x509_local_key);
                assert.equal('string', data.response.x509_local_cert);
                assert.equal('string', data.response.x509_local_cert_subject);
                assert.equal('string', data.response.x509_remote_auth);
                assert.equal('string', data.response.x509_remote_cert);
                assert.equal('string', data.response.x509_remote_cert_subject);
                assert.equal('string', data.response.x509_remote_ca);
                assert.equal('string', data.response.x509_remote_ca_subject);
                assert.equal(6, data.response.ikev);
                assert.equal(false, data.response.aggressive);
                assert.equal('string', data.response.ikeenc);
                assert.equal('string', data.response.ikehash);
                assert.equal('string', data.response.ipsecenc);
                assert.equal('string', data.response.ipsechash);
                assert.equal('string', data.response.ikedh);
                assert.equal('string', data.response.ipsecdh);
                assert.equal(9, data.response.ike_lifetime);
                assert.equal(3, data.response.ipsec_lifetime);
                assert.equal(true, data.response.compress);
                assert.equal(true, data.response.reqid);
                assert.equal('string', data.response.leftid);
                assert.equal('string', data.response.rightid);
                assert.equal(true, data.response.fallback);
                assert.equal(false, data.response.inactive);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cvpn', 'postOrgOrgidCvpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cvpnCvpnid = 'fakedata';
    describe('#putCvpnCvpnid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCvpnCvpnid(cvpnCvpnid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cvpn', 'putCvpnCvpnid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCvpnCvpnid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCvpnCvpnid(cvpnCvpnid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.uplink);
                assert.equal(true, Array.isArray(data.response.nets));
                assert.equal('SiteName-cvpn', data.response.name);
                assert.equal('string', data.response.uid);
                assert.equal('string', data.response.dev);
                assert.equal('string', data.response.rgw);
                assert.equal('string', data.response.auth);
                assert.equal('string', data.response.psk);
                assert.equal('string', data.response.x509_local_type);
                assert.equal('string', data.response.x509_local_key);
                assert.equal('string', data.response.x509_local_cert);
                assert.equal('string', data.response.x509_local_cert_subject);
                assert.equal('string', data.response.x509_remote_auth);
                assert.equal('string', data.response.x509_remote_cert);
                assert.equal('string', data.response.x509_remote_cert_subject);
                assert.equal('string', data.response.x509_remote_ca);
                assert.equal('string', data.response.x509_remote_ca_subject);
                assert.equal(4, data.response.ikev);
                assert.equal(false, data.response.aggressive);
                assert.equal('string', data.response.ikeenc);
                assert.equal('string', data.response.ikehash);
                assert.equal('string', data.response.ipsecenc);
                assert.equal('string', data.response.ipsechash);
                assert.equal('string', data.response.ikedh);
                assert.equal('string', data.response.ipsecdh);
                assert.equal(5, data.response.ike_lifetime);
                assert.equal(6, data.response.ipsec_lifetime);
                assert.equal(true, data.response.compress);
                assert.equal(true, data.response.reqid);
                assert.equal('string', data.response.leftid);
                assert.equal('string', data.response.rightid);
                assert.equal(true, data.response.fallback);
                assert.equal(true, data.response.inactive);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cvpn', 'getCvpnCvpnid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cvpnNetid = 'fakedata';
    describe('#putCvpnCvpnidNetid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCvpnCvpnidNetid(cvpnCvpnid, cvpnNetid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cvpn', 'putCvpnCvpnidNetid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCvpnCvpnidNetid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCvpnCvpnidNetid(cvpnCvpnid, cvpnNetid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.net);
                assert.equal('string', data.response.cvpn);
                assert.equal('string', data.response.natnetv4);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cvpn', 'getCvpnCvpnidNetid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCvpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCvpns((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cvpn', 'getCvpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCvpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidCvpns(cvpnOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cvpn', 'getOrgOrgidCvpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const areaOrgid = 'fakedata';
    const areaPostOrgOrgidAreasBodyParam = {
      name: 'string',
      id: 'string',
      org: 'string',
      ospfnet: 'string',
      site: 'string',
      segments: [
        'string'
      ],
      uplinks: [
        'string'
      ],
      type: 'string',
      backbone: false,
      area_id: 'string',
      password: 'string',
      auth_method: false,
      md5_key_id: 7,
      hello_interval: 3,
      dead_interval: 5,
      priority: 2,
      cost: 9
    };
    describe('#postOrgOrgidAreas - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidAreas(areaOrgid, areaPostOrgOrgidAreasBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.ospfnet);
                assert.equal('string', data.response.site);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.backbone);
                assert.equal('string', data.response.area_id);
                assert.equal('string', data.response.password);
                assert.equal(true, data.response.auth_method);
                assert.equal(4, data.response.md5_key_id);
                assert.equal(10, data.response.hello_interval);
                assert.equal(10, data.response.dead_interval);
                assert.equal(4, data.response.priority);
                assert.equal(7, data.response.cost);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Area', 'postOrgOrgidAreas', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const areaAreaid = 'fakedata';
    describe('#putAreaAreaid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAreaAreaid(areaAreaid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Area', 'putAreaAreaid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAreaAreaid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAreaAreaid(areaAreaid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.ospfnet);
                assert.equal('string', data.response.site);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.backbone);
                assert.equal('string', data.response.area_id);
                assert.equal('string', data.response.password);
                assert.equal(false, data.response.auth_method);
                assert.equal(4, data.response.md5_key_id);
                assert.equal(6, data.response.hello_interval);
                assert.equal(7, data.response.dead_interval);
                assert.equal(2, data.response.priority);
                assert.equal(8, data.response.cost);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Area', 'getAreaAreaid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAreas - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAreas((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Area', 'getAreas', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidAreas - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidAreas(areaOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Area', 'getOrgOrgidAreas', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRealm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRealm((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Realm', 'putRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRealm((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Realm', 'getRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const shprimaryShprimaryid = 'fakedata';
    describe('#putShprimaryShprimaryid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putShprimaryShprimaryid(shprimaryShprimaryid, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shprimary', 'putShprimaryShprimaryid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let shprimaryShPrimaryIpv4 = 'fakedata';
    let shprimaryShPrimaryGwv4 = 'fakedata';
    let shprimaryNet = 'fakedata';
    describe('#getShprimaryShprimaryid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getShprimaryShprimaryid(shprimaryShprimaryid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('shprimary-45a4daaa5c988347', data.response.id);
                assert.equal('10.20.30.40/24', data.response.shPrimaryIpv4);
                assert.equal('1.2.3.4', data.response.shPrimaryGwv4);
                assert.equal('net-Net-2e8cba8f8e39ae08', data.response.net);
                assert.equal('node-d7ceafbda4ebbe70', data.response.node);
                assert.equal('org-RVBD-d7ceafbda4ebbe70', data.response.org);
              } else {
                runCommonAsserts(data, error);
              }
              shprimaryShPrimaryIpv4 = data.response.shPrimaryIpv4;
              shprimaryShPrimaryGwv4 = data.response.shPrimaryGwv4;
              shprimaryNet = data.response.net;
              saveMockData('Shprimary', 'getShprimaryShprimaryid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inpathInpathid = 'fakedata';
    describe('#putInpathInpathid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putInpathInpathid(inpathInpathid, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inpath', 'putInpathInpathid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let inpathIpv4 = 'fakedata';
    let inpathNet = 'fakedata';
    describe('#getInpathInpathid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInpathInpathid(inpathInpathid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('10.20.30.40/24', data.response.ipv4);
                assert.equal('net-Net-2e8cba8f8e39ae08', data.response.net);
                assert.equal('node-d7ceafbda4ebbe70', data.response.node);
                assert.equal('org-RVBD-d7ceafbda4ebbe70', data.response.org);
              } else {
                runCommonAsserts(data, error);
              }
              inpathIpv4 = data.response.ipv4;
              inpathNet = data.response.net;
              saveMockData('Inpath', 'getInpathInpathid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cepolicyOrgid = 'fakedata';
    const cepolicyPostOrgOrgidCepolicyBodyParam = {
      name: 'Cepolicy-1',
      site: 'string'
    };
    describe('#postOrgOrgidCepolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidCepolicy(cepolicyOrgid, cepolicyPostOrgOrgidCepolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Cepolicy-1', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.exportpolices));
                assert.equal(true, Array.isArray(data.response.importpolicies));
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.site);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cepolicy', 'postOrgOrgidCepolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCepolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCepolicies((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cepolicy', 'getCepolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cepolicyCepolicyid = 'fakedata';
    const cepolicyPutCepoliciesCepolicyidBodyParam = {
      name: 'Cepolicy-1',
      site: 'string'
    };
    describe('#putCepoliciesCepolicyid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCepoliciesCepolicyid(cepolicyCepolicyid, cepolicyPutCepoliciesCepolicyidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cepolicy', 'putCepoliciesCepolicyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCepoliciesCepolicyid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCepoliciesCepolicyid(cepolicyCepolicyid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Cepolicy-1', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.exportpolices));
                assert.equal(true, Array.isArray(data.response.importpolicies));
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.site);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cepolicy', 'getCepoliciesCepolicyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCepolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidCepolicy(cepolicyOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cepolicy', 'getOrgOrgidCepolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cefilterOrgid = 'fakedata';
    const cefilterPostOrgOrgidCefiltersBodyParam = {
      name: 'string'
    };
    describe('#postOrgOrgidCefilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidCefilters(cefilterOrgid, cefilterPostOrgOrgidCefiltersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.category);
                assert.equal('string', data.response.match_type);
                assert.equal('string', data.response.action_type);
                assert.equal(true, Array.isArray(data.response.prefixes));
                assert.equal('string', data.response.block_route);
                assert.equal('string', data.response.set_type);
                assert.equal('string', data.response.metric_type);
                assert.equal(5, data.response.metric_value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cefilter', 'postOrgOrgidCefilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCefilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCefilters((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cefilter', 'getCefilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cefilterCefilterid = 'fakedata';
    const cefilterPutCefiltersCefilteridBodyParam = {
      name: 'string'
    };
    describe('#putCefiltersCefilterid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCefiltersCefilterid(cefilterCefilterid, cefilterPutCefiltersCefilteridBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cefilter', 'putCefiltersCefilterid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCefiltersCefilterid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCefiltersCefilterid(cefilterCefilterid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.site);
                assert.equal('string', data.response.category);
                assert.equal('string', data.response.match_type);
                assert.equal('string', data.response.action_type);
                assert.equal(true, Array.isArray(data.response.prefixes));
                assert.equal('string', data.response.block_route);
                assert.equal('string', data.response.set_type);
                assert.equal('string', data.response.metric_type);
                assert.equal(4, data.response.metric_value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cefilter', 'getCefiltersCefilterid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCefilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidCefilters(cefilterOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cefilter', 'getOrgOrgidCefilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const prefixOrgid = 'fakedata';
    const prefixPostOrgOrgidPrefixBodyParam = {
      prefix: 'string'
    };
    describe('#postOrgOrgidPrefix - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidPrefix(prefixOrgid, prefixPostOrgOrgidPrefixBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.summary);
                assert.equal('string', data.response.prefix);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.ceexportpolicy);
                assert.equal('string', data.response.ceimportpolicy);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.cefilter);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefix', 'postOrgOrgidPrefix', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidPrefix - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidPrefix(prefixOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefix', 'getOrgOrgidPrefix', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefixes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPrefixes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefix', 'getPrefixes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const prefixPrefixid = 'fakedata';
    const prefixPutPrefixesPrefixidBodyParam = {
      prefix: 'string'
    };
    describe('#putPrefixesPrefixid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putPrefixesPrefixid(prefixPrefixid, prefixPutPrefixesPrefixidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefix', 'putPrefixesPrefixid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefixesPrefixid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPrefixesPrefixid(prefixPrefixid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.summary);
                assert.equal('string', data.response.prefix);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.ceexportpolicy);
                assert.equal('string', data.response.ceimportpolicy);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.cefilter);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefix', 'getPrefixesPrefixid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ceexportruleOrgid = 'fakedata';
    const ceexportrulePostOrgOrgidCeexportrulesBodyParam = {
      id: 'string',
      org: 'string',
      ceexportpolicy: 'string',
      default_originate: true,
      filters: [
        'string'
      ],
      uplinks: [
        'string'
      ]
    };
    describe('#postOrgOrgidCeexportrules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidCeexportrules(ceexportruleOrgid, ceexportrulePostOrgOrgidCeexportrulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.ceexportpolicy);
                assert.equal(false, data.response.default_originate);
                assert.equal(true, Array.isArray(data.response.filters));
                assert.equal(true, Array.isArray(data.response.uplinks));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportrule', 'postOrgOrgidCeexportrules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeexportrules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCeexportrules((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportrule', 'getCeexportrules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ceexportruleCeexportruleid = 'fakedata';
    const ceexportrulePutCeexportrulesCeexportruleidBodyParam = {
      id: 'string',
      org: 'string',
      ceexportpolicy: 'string',
      default_originate: false,
      filters: [
        'string'
      ],
      uplinks: [
        'string'
      ]
    };
    describe('#putCeexportrulesCeexportruleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCeexportrulesCeexportruleid(ceexportruleCeexportruleid, ceexportrulePutCeexportrulesCeexportruleidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportrule', 'putCeexportrulesCeexportruleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeexportrulesCeexportruleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCeexportrulesCeexportruleid(ceexportruleCeexportruleid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.org);
                assert.equal('string', data.response.ceexportpolicy);
                assert.equal(true, data.response.default_originate);
                assert.equal(true, Array.isArray(data.response.filters));
                assert.equal(true, Array.isArray(data.response.uplinks));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportrule', 'getCeexportrulesCeexportruleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCeexportrules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidCeexportrules(ceexportruleOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportrule', 'getOrgOrgidCeexportrules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ceexportpolicyOrgid = 'fakedata';
    const ceexportpolicyPostOrgOrgidCeexportpoliciesBodyParam = {
      id: 'string',
      cepolicy: 'string',
      wan_prefixes: [
        'string'
      ],
      exportrules: [
        'string'
      ],
      redistribute: true
    };
    describe('#postOrgOrgidCeexportpolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidCeexportpolicies(ceexportpolicyOrgid, ceexportpolicyPostOrgOrgidCeexportpoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.cepolicy);
                assert.equal(true, Array.isArray(data.response.wan_prefixes));
                assert.equal(true, Array.isArray(data.response.exportrules));
                assert.equal(false, data.response.redistribute);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportpolicy', 'postOrgOrgidCeexportpolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeexportpolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCeexportpolicies((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportpolicy', 'getCeexportpolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ceexportpolicyCeexportpolicyid = 'fakedata';
    const ceexportpolicyPutCeexportpoliciesCeexportpolicyidBodyParam = {
      id: 'string',
      cepolicy: 'string',
      wan_prefixes: [
        'string'
      ],
      exportrules: [
        'string'
      ],
      redistribute: true
    };
    describe('#putCeexportpoliciesCeexportpolicyid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCeexportpoliciesCeexportpolicyid(ceexportpolicyCeexportpolicyid, ceexportpolicyPutCeexportpoliciesCeexportpolicyidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportpolicy', 'putCeexportpoliciesCeexportpolicyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeexportpoliciesCeexportpolicyid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCeexportpoliciesCeexportpolicyid(ceexportpolicyCeexportpolicyid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.cepolicy);
                assert.equal(true, Array.isArray(data.response.wan_prefixes));
                assert.equal(true, Array.isArray(data.response.exportrules));
                assert.equal(true, data.response.redistribute);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportpolicy', 'getCeexportpoliciesCeexportpolicyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCeexportpolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidCeexportpolicies(ceexportpolicyOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportpolicy', 'getOrgOrgidCeexportpolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ceimportpolicyOrgid = 'fakedata';
    const ceimportpolicyPostOrgOrgidCeimportpoliciesBodyParam = {
      id: 'string',
      redistribute: true,
      default_originate: true,
      ospfnet: [
        'string'
      ],
      filters: [
        'string'
      ],
      uplinks: [
        'string'
      ],
      lan_prefixes: [
        'string'
      ],
      cepolicy: 'string'
    };
    describe('#postOrgOrgidCeimportpolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrgOrgidCeimportpolicies(ceimportpolicyOrgid, ceimportpolicyPostOrgOrgidCeimportpoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.redistribute);
                assert.equal(true, data.response.default_originate);
                assert.equal(true, Array.isArray(data.response.ospfnet));
                assert.equal(true, Array.isArray(data.response.filters));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal(true, Array.isArray(data.response.lan_prefixes));
                assert.equal('string', data.response.cepolicy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceimportpolicy', 'postOrgOrgidCeimportpolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeimportpolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCeimportpolicies((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceimportpolicy', 'getCeimportpolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ceimportpolicyCeimportpolicyid = 'fakedata';
    const ceimportpolicyPutCeimportpoliciesCeimportpolicyidBodyParam = {
      id: 'string',
      redistribute: true,
      default_originate: false,
      ospfnet: [
        'string'
      ],
      filters: [
        'string'
      ],
      uplinks: [
        'string'
      ],
      lan_prefixes: [
        'string'
      ],
      cepolicy: 'string'
    };
    describe('#putCeimportpoliciesCeimportpolicyid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCeimportpoliciesCeimportpolicyid(ceimportpolicyCeimportpolicyid, ceimportpolicyPutCeimportpoliciesCeimportpolicyidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceimportpolicy', 'putCeimportpoliciesCeimportpolicyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeimportpoliciesCeimportpolicyid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCeimportpoliciesCeimportpolicyid(ceimportpolicyCeimportpolicyid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(false, data.response.redistribute);
                assert.equal(false, data.response.default_originate);
                assert.equal(true, Array.isArray(data.response.ospfnet));
                assert.equal(true, Array.isArray(data.response.filters));
                assert.equal(true, Array.isArray(data.response.uplinks));
                assert.equal(true, Array.isArray(data.response.lan_prefixes));
                assert.equal('string', data.response.cepolicy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceimportpolicy', 'getCeimportpoliciesCeimportpolicyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCeimportpolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidCeimportpolicies(ceimportpolicyOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceimportpolicy', 'getOrgOrgidCeimportpolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudDeploySiteid = 'fakedata';
    const cloudDeployPostSiteSiteidCloudDeployBodyParam = {
      siteid: 'site-abcd1234'
    };
    describe('#postSiteSiteidCloudDeploy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSiteSiteidCloudDeploy(cloudDeploySiteid, cloudDeployPostSiteSiteidCloudDeployBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudDeploy', 'postSiteSiteidCloudDeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudDeployPutSiteSiteidCloudDeployBodyParam = {
      siteid: 'site-abcd1234'
    };
    describe('#putSiteSiteidCloudDeploy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSiteSiteidCloudDeploy(cloudDeploySiteid, cloudDeployPutSiteSiteidCloudDeployBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudDeploy', 'putSiteSiteidCloudDeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSiteidCloudDeploy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteSiteidCloudDeploy(cloudDeploySiteid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudDeploy', 'getSiteSiteidCloudDeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudAccountOrgid = 'fakedata';
    const cloudAccountPostOrgOrgidCloudAccountsBodyParam = {
      'account-name': 'scm-account-aws',
      'subscription-id': 'string',
      'application-id': 'string',
      'secret-key': 'string',
      'tenant-id': 'string',
      'access-key': 'string',
      'account-type': 'AWS',
      'aws-account-type': 'credentials'
    };
    describe('#postOrgOrgidCloudAccounts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postOrgOrgidCloudAccounts(cloudAccountOrgid, cloudAccountPostOrgOrgidCloudAccountsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudAccount', 'postOrgOrgidCloudAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudAccountAccountId = 'fakedata';
    const cloudAccountPutCloudAccountAccountIdBodyParam = {
      'account-name': 'scm-account-aws',
      'subscription-id': 'string',
      'application-id': 'string',
      'secret-key': 'string',
      'tenant-id': 'string',
      'access-key': 'string',
      'account-type': 'AWS',
      'aws-account-type': 'credentials'
    };
    describe('#putCloudAccountAccountId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCloudAccountAccountId(cloudAccountPutCloudAccountAccountIdBodyParam, cloudAccountAccountId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudAccount', 'putCloudAccountAccountId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudAccountAccountId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudAccountAccountId(cloudAccountAccountId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('scm-account-aws', data.response['account-name']);
                assert.equal('string', data.response['subscription-id']);
                assert.equal('string', data.response['application-id']);
                assert.equal('string', data.response['secret-key']);
                assert.equal('string', data.response['tenant-id']);
                assert.equal('string', data.response['access-key']);
                assert.equal('AWS', data.response['account-type']);
                assert.equal('credentials', data.response['aws-account-type']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudAccount', 'getCloudAccountAccountId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudAccountAccountIdRefresh - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudAccountAccountIdRefresh(cloudAccountAccountId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudAccount', 'getCloudAccountAccountIdRefresh', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCloudAccounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidCloudAccounts(cloudAccountOrgid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudAccount', 'getOrgOrgidCloudAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subnetOrgid = 'fakedata';
    const subnetAccountId = 'fakedata';
    const subnetSubnetId = 'fakedata';
    const subnetPostOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetIdBodyParam = {
      'subnet-id': 'string'
    };
    describe('#postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId(subnetOrgid, subnetAccountId, subnetSubnetId, subnetPostOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnet', 'postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subnetVnetId = 'fakedata';
    const subnetPostOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetIdBodyParam = {
      'subnet-id': 'string'
    };
    describe('#postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId(subnetOrgid, subnetAccountId, subnetVnetId, subnetPostOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnet', 'postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subnetVpcId = 'fakedata';
    const subnetPostOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcIdBodyParam = {
      'subnet-id': 'string'
    };
    describe('#postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId(subnetOrgid, subnetAccountId, subnetVpcId, subnetPostOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnet', 'postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCloudAccountAccountIdCloudConnectSubnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgOrgidCloudAccountAccountIdCloudConnectSubnets(subnetOrgid, subnetAccountId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnet', 'getOrgOrgidCloudAccountAccountIdCloudConnectSubnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgOrgid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrgOrgid(organizationOrgid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'deleteOrgOrgid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBgpneighsBgpneighid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBgpneighsBgpneighid(bgpneighBgpneighid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgpneigh', 'deleteBgpneighsBgpneighid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcinterfacesDcinterfaceid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcinterfacesDcinterfaceid(dcinterfaceDcinterfaceid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcinterface', 'deleteDcinterfacesDcinterfaceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUplinkUplinkid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUplinkUplinkid(uplinkUplinkid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Uplink', 'deleteUplinkUplinkid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteSiteid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSiteSiteid(siteSiteid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'deleteSiteSiteid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitegroupSitegroupid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitegroupSitegroupid(sitegroupSitegroupid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sitegroup', 'deleteSitegroupSitegroupid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneZoneid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteZoneZoneid(zoneZoneid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zone', 'deleteZoneZoneid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSwitchSwitchid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSwitchSwitchid(switchSwitchid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch', 'deleteSwitchSwitchid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApApid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApApid(accesspointApid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accesspoint', 'deleteApApid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodeNodeid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNodeNodeid(nodeNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Node', 'deleteNodeNodeid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClusterClusterid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteClusterClusterid(clusterClusterid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cluster', 'deleteClusterClusterid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcuplinkDcuplinkid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcuplinkDcuplinkid(dcuplinkDcuplinkid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcuplink', 'deleteDcuplinkDcuplinkid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSsidSsidid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSsidSsidid(ssidSsidid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssid', 'deleteSsidSsidid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBroadcastBcastid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBroadcastBcastid(broadcastBcastid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Broadcast', 'deleteBroadcastBcastid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomAppAppid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomAppAppid(appAppid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'deleteCustomAppAppid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppGroupAppgrpid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAppGroupAppgrpid(appgrpAppgrpid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appgrp', 'deleteAppGroupAppgrpid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserUserid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUserUserid(userUserid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'deleteUserUserid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceDevid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceDevid(deviceDevid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'deleteDeviceDevid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePathRulePruleid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePathRulePruleid(pathrulePruleid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pathrule', 'deletePathRulePruleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOutboundRuleRuleid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOutboundRuleRuleid(outboundruleRuleid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outboundrule', 'deleteOutboundRuleRuleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInboundRuleRuleid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInboundRuleRuleid(inboundruleRuleid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inboundrule', 'deleteInboundRuleRuleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEndpointEpid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEndpointEpid(endpointEpid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Endpoint', 'deleteEndpointEpid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkNetid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkNetid(networkNetid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'deleteNetworkNetid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWanWanid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWanWanid(wanWanid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wan', 'deleteWanWanid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSshtunnelNodeid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSshtunnelNodeid(sshtunnelNodeid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshtunnel', 'deleteSshtunnelNodeid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProxyserviceProxyserviceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteProxyserviceProxyserviceid(proxyserviceProxyserviceid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Proxyservice', 'deleteProxyserviceProxyserviceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsergrpUsergrpid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsergrpUsergrpid(usergrpUsergrpid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Usergrp', 'deleteUsergrpUsergrpid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortalPortalid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePortalPortalid(portalPortalid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Portal', 'deletePortalPortalid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOspfnetOspfnetid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOspfnetOspfnetid(ospfnetOspfnetid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospfnet', 'deleteOspfnetOspfnetid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRollingUpgradeScheduleid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRollingUpgradeScheduleid(rollingUpgradeScheduleid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RollingUpgrade', 'deleteRollingUpgradeScheduleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWifiPlanWifiPlanid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWifiPlanWifiPlanid(wifiPlanWifiPlanid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WifiPlan', 'deleteWifiPlanWifiPlanid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCvpnCvpnid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCvpnCvpnid(cvpnCvpnid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cvpn', 'deleteCvpnCvpnid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCvpnCvpnidNetid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCvpnCvpnidNetid(cvpnCvpnid, cvpnNetid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cvpn', 'deleteCvpnCvpnidNetid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAreaAreaid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAreaAreaid(areaAreaid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Area', 'deleteAreaAreaid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCepoliciesCepolicyid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCepoliciesCepolicyid(cepolicyCepolicyid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cepolicy', 'deleteCepoliciesCepolicyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCefiltersCefilterid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCefiltersCefilterid(cefilterCefilterid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cefilter', 'deleteCefiltersCefilterid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePrefixesPrefixid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePrefixesPrefixid(prefixPrefixid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefix', 'deletePrefixesPrefixid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCeexportrulesCeexportruleid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCeexportrulesCeexportruleid(ceexportruleCeexportruleid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportrule', 'deleteCeexportrulesCeexportruleid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCeexportpoliciesCeexportpolicyid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCeexportpoliciesCeexportpolicyid(ceexportpolicyCeexportpolicyid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceexportpolicy', 'deleteCeexportpoliciesCeexportpolicyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCeimportpoliciesCeimportpolicyid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCeimportpoliciesCeimportpolicyid(ceimportpolicyCeimportpolicyid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ceimportpolicy', 'deleteCeimportpoliciesCeimportpolicyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteSiteidCloudDeploy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSiteSiteidCloudDeploy(cloudDeploySiteid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudDeploy', 'deleteSiteSiteidCloudDeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCloudAccountAccountId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCloudAccountAccountId(cloudAccountAccountId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudAccount', 'deleteCloudAccountAccountId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId(subnetOrgid, subnetAccountId, subnetSubnetId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnet', 'deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId(subnetOrgid, subnetAccountId, subnetVnetId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnet', 'deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId(subnetOrgid, subnetAccountId, subnetVpcId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-steel_connect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnet', 'deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
