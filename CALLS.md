## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for SteelConnect. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for SteelConnect.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Riverbed SteelConnect. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getStatus(callback)</td>
    <td style="padding:15px">SCM status information</td>
    <td style="padding:15px">{base_path}/{version}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgs(callback)</td>
    <td style="padding:15px">List organizations</td>
    <td style="padding:15px">{base_path}/{version}/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgs(org, callback)</td>
    <td style="padding:15px">Create a new organization</td>
    <td style="padding:15px">{base_path}/{version}/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgid(orgid, callback)</td>
    <td style="padding:15px">Get organization info</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOrgOrgid(orgid, org, callback)</td>
    <td style="padding:15px">Update an organization object</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgOrgid(orgid, callback)</td>
    <td style="padding:15px">Delete organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBgpneighs(callback)</td>
    <td style="padding:15px">List BGP neighbors</td>
    <td style="padding:15px">{base_path}/{version}/bgpneighs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBgpneighsBgpneighid(bgpneighid, callback)</td>
    <td style="padding:15px">Get bgp neighbor info</td>
    <td style="padding:15px">{base_path}/{version}/bgpneighs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBgpneighsBgpneighid(bgpneighid, callback)</td>
    <td style="padding:15px">Delete bgp neighbor</td>
    <td style="padding:15px">{base_path}/{version}/bgpneighs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBgpneighsBgpneighid(bgpneighid, callback)</td>
    <td style="padding:15px">Update an existing BGP neighbor</td>
    <td style="padding:15px">{base_path}/{version}/bgpneighs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidBgpneighs(orgid, callback)</td>
    <td style="padding:15px">List all BGP neighbors in organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/bgpneighs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidBgpneighs(orgid, callback)</td>
    <td style="padding:15px">Create a new BGP neighbor in a specified organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/bgpneighs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcinterfaces(callback)</td>
    <td style="padding:15px">List datacenter interfaces</td>
    <td style="padding:15px">{base_path}/{version}/dcinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcinterfacesDcinterfaceid(dcinterfaceid, callback)</td>
    <td style="padding:15px">Get datacenter interface info</td>
    <td style="padding:15px">{base_path}/{version}/dcinterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcinterfacesDcinterfaceid(dcinterfaceid, callback)</td>
    <td style="padding:15px">Delete datacenter interface</td>
    <td style="padding:15px">{base_path}/{version}/dcinterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcinterfacesDcinterfaceid(dcinterfaceid, callback)</td>
    <td style="padding:15px">Update a datacenter interface in a specified organization.</td>
    <td style="padding:15px">{base_path}/{version}/dcinterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidDcinterfaces(orgid, callback)</td>
    <td style="padding:15px">List all datacenter interfaces in organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/dcinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidDcinterfaces(orgid, callback)</td>
    <td style="padding:15px">Create a datacenter interface in a specified organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/dcinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUplinks(callback)</td>
    <td style="padding:15px">Retrieve all uplinks</td>
    <td style="padding:15px">{base_path}/{version}/uplinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUplinkUplinkid(uplinkid, callback)</td>
    <td style="padding:15px">Retrieve specified uplink</td>
    <td style="padding:15px">{base_path}/{version}/uplink/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUplinkUplinkid(uplinkid, callback)</td>
    <td style="padding:15px">Update a uplink.</td>
    <td style="padding:15px">{base_path}/{version}/uplink/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUplinkUplinkid(uplinkid, callback)</td>
    <td style="padding:15px">Delete uplink</td>
    <td style="padding:15px">{base_path}/{version}/uplink/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidUplinks(orgid, callback)</td>
    <td style="padding:15px">List uplinks for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/uplinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidUplinks(orgid, uplink, callback)</td>
    <td style="padding:15px">Create uplink</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/uplinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSiteidUplinks(siteid, callback)</td>
    <td style="padding:15px">List uplinks for given site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/uplinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUplinkUplinkidState(uplinkid, body, callback)</td>
    <td style="padding:15px">Change the state of an uplink.</td>
    <td style="padding:15px">{base_path}/{version}/uplink/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSites(callback)</td>
    <td style="padding:15px">Retrieve all sites</td>
    <td style="padding:15px">{base_path}/{version}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidSites(orgid, callback)</td>
    <td style="padding:15px">List sites for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidSites(orgid, site, callback)</td>
    <td style="padding:15px">Create site</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSiteid(siteid, callback)</td>
    <td style="padding:15px">Retrieve specified site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSiteSiteid(siteid, callback)</td>
    <td style="padding:15px">Update a site.</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteSiteid(siteid, callback)</td>
    <td style="padding:15px">Delete site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitegroups(callback)</td>
    <td style="padding:15px">Retrieve all sitegroups</td>
    <td style="padding:15px">{base_path}/{version}/sitegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidSitegroups(orgid, callback)</td>
    <td style="padding:15px">List sitegroups for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/sitegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidSitegroups(orgid, sitegroup, callback)</td>
    <td style="padding:15px">Create sitegroup</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/sitegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitegroupSitegroupid(sitegroupid, callback)</td>
    <td style="padding:15px">Retrieve specified sitegroup</td>
    <td style="padding:15px">{base_path}/{version}/sitegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSitegroupSitegroupid(sitegroupid, callback)</td>
    <td style="padding:15px">Update a sitegroup.</td>
    <td style="padding:15px">{base_path}/{version}/sitegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitegroupSitegroupid(sitegroupid, callback)</td>
    <td style="padding:15px">Delete sitegroup</td>
    <td style="padding:15px">{base_path}/{version}/sitegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZones(callback)</td>
    <td style="padding:15px">Retrieve all zones</td>
    <td style="padding:15px">{base_path}/{version}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneZoneid(zoneid, callback)</td>
    <td style="padding:15px">Retrieve specified zone</td>
    <td style="padding:15px">{base_path}/{version}/zone/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putZoneZoneid(zoneid, callback)</td>
    <td style="padding:15px">Update a zone.</td>
    <td style="padding:15px">{base_path}/{version}/zone/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZoneZoneid(zoneid, callback)</td>
    <td style="padding:15px">Delete zone</td>
    <td style="padding:15px">{base_path}/{version}/zone/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidZones(orgid, callback)</td>
    <td style="padding:15px">List zones for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidZones(orgid, zone, callback)</td>
    <td style="padding:15px">Create zone</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSiteidZones(siteid, callback)</td>
    <td style="padding:15px">List zones for given site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitches(callback)</td>
    <td style="padding:15px">List all switches</td>
    <td style="padding:15px">{base_path}/{version}/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidSwitches(orgid, callback)</td>
    <td style="padding:15px">List switches for given org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidSwitches(orgid, switchParam, callback)</td>
    <td style="padding:15px">Create a new switch</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchSwitchid(switchid, callback)</td>
    <td style="padding:15px">Get switch object</td>
    <td style="padding:15px">{base_path}/{version}/switch/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSwitchSwitchid(switchid, callback)</td>
    <td style="padding:15px">Update a switch.</td>
    <td style="padding:15px">{base_path}/{version}/switch/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSwitchSwitchid(switchid, callback)</td>
    <td style="padding:15px">Delete switch</td>
    <td style="padding:15px">{base_path}/{version}/switch/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAp(callback)</td>
    <td style="padding:15px">List all access points</td>
    <td style="padding:15px">{base_path}/{version}/ap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidAp(orgid, callback)</td>
    <td style="padding:15px">List access points</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidAp(orgid, ap, callback)</td>
    <td style="padding:15px">Create a new access point</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApApid(apid, callback)</td>
    <td style="padding:15px">Get access point info</td>
    <td style="padding:15px">{base_path}/{version}/ap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putApApid(apid, callback)</td>
    <td style="padding:15px">Update a access point.</td>
    <td style="padding:15px">{base_path}/{version}/ap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApApid(apid, callback)</td>
    <td style="padding:15px">Delete access point</td>
    <td style="padding:15px">{base_path}/{version}/ap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodes(changedSince, callback)</td>
    <td style="padding:15px">List all nodes</td>
    <td style="padding:15px">{base_path}/{version}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeNodeidImageStatus(nodeid, callback)</td>
    <td style="padding:15px">Get the status of appliance image request</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/image_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeNodeidGetImage(file, nodeid, callback)</td>
    <td style="padding:15px">Gets the generated appliance image</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/get_image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeNodeid(nodeid, callback)</td>
    <td style="padding:15px">Get node object</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeNodeid(nodeid, callback)</td>
    <td style="padding:15px">Update a node.</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodeNodeid(nodeid, callback)</td>
    <td style="padding:15px">Delete node</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeNodeidRemove(nodeid, callback)</td>
    <td style="padding:15px">Remove a node</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeNodeidFactoryReset(nodeid, callback)</td>
    <td style="padding:15px">Factory reset a node</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/factory_reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeNodeidReboot(nodeid, callback)</td>
    <td style="padding:15px">Reboot a node</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/reboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidNodeRegister(orgid, node, callback)</td>
    <td style="padding:15px">Registers a hardware appliance</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/node/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidNodeVirtualRegister(orgid, node, callback)</td>
    <td style="padding:15px">Registers a virtual appliance</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/node/virtual/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeNodeidPrepareImage(nodeid, type, callback)</td>
    <td style="padding:15px">Creates virtual node image</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/prepare_image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeNodeidGenerateSupportPackage(nodeid, callback)</td>
    <td style="padding:15px">Generate the support package for a node</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/generate_support_package?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeNodeidDownloadSupportPackage(nodeid, callback)</td>
    <td style="padding:15px">Download the support package generated from generate_support_package call</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/download_support_package?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeNodeidInventoryUpdate(nodeid, callback)</td>
    <td style="padding:15px">Trigger the Inventory Update action on the node</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/inventory_update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSiteidNodes(siteid, callback)</td>
    <td style="padding:15px">List nodes</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidNodes(orgid, callback)</td>
    <td style="padding:15px">List nodes</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeNodeidRetryUpgrade(nodeid, callback)</td>
    <td style="padding:15px">Trigger upgrade retrial on node.</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/retry_upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusters(callback)</td>
    <td style="padding:15px">List all clusters</td>
    <td style="padding:15px">{base_path}/{version}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterClusterid(clusterid, callback)</td>
    <td style="padding:15px">Get cluster object</td>
    <td style="padding:15px">{base_path}/{version}/cluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putClusterClusterid(clusterid, cluster, callback)</td>
    <td style="padding:15px">Update a cluster object</td>
    <td style="padding:15px">{base_path}/{version}/cluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClusterClusterid(clusterid, callback)</td>
    <td style="padding:15px">Delete cluster</td>
    <td style="padding:15px">{base_path}/{version}/cluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSiteidClusters(siteid, callback)</td>
    <td style="padding:15px">List clusters</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidClusters(orgid, callback)</td>
    <td style="padding:15px">List clusters</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidClusters(orgid, cluster, callback)</td>
    <td style="padding:15px">Create a new cluster</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcuplinks(callback)</td>
    <td style="padding:15px">List all dcuplinks</td>
    <td style="padding:15px">{base_path}/{version}/dcuplinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcuplinkDcuplinkid(dcuplinkid, callback)</td>
    <td style="padding:15px">Get dcuplink object</td>
    <td style="padding:15px">{base_path}/{version}/dcuplink/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcuplinkDcuplinkid(dcuplinkid, dcuplink, callback)</td>
    <td style="padding:15px">Update a dcuplink object</td>
    <td style="padding:15px">{base_path}/{version}/dcuplink/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcuplinkDcuplinkid(dcuplinkid, callback)</td>
    <td style="padding:15px">Delete dcuplink</td>
    <td style="padding:15px">{base_path}/{version}/dcuplink/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidDcuplinks(orgid, callback)</td>
    <td style="padding:15px">List dcuplinks</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/dcuplinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidDcuplinks(orgid, dcuplink, callback)</td>
    <td style="padding:15px">Create a new dcuplink</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/dcuplinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSsids(callback)</td>
    <td style="padding:15px">List all ssids</td>
    <td style="padding:15px">{base_path}/{version}/ssids?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidSsids(orgid, callback)</td>
    <td style="padding:15px">List all ssids within organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ssids?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidSsids(orgid, ssid, callback)</td>
    <td style="padding:15px">Create a new ssid</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ssids?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSsidSsidid(ssidid, callback)</td>
    <td style="padding:15px">Get ssid</td>
    <td style="padding:15px">{base_path}/{version}/ssid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSsidSsidid(ssidid, callback)</td>
    <td style="padding:15px">Update a ssid.</td>
    <td style="padding:15px">{base_path}/{version}/ssid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSsidSsidid(ssidid, callback)</td>
    <td style="padding:15px">Delete ssid</td>
    <td style="padding:15px">{base_path}/{version}/ssid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBroadcasts(callback)</td>
    <td style="padding:15px">List all broadcasts</td>
    <td style="padding:15px">{base_path}/{version}/broadcasts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidBroadcasts(orgid, callback)</td>
    <td style="padding:15px">List broadcasts</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/broadcasts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidBroadcasts(orgid, broadcast, callback)</td>
    <td style="padding:15px">Create a new broadcast</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/broadcasts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSiteidBroadcasts(siteid, callback)</td>
    <td style="padding:15px">List broadcasts</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/broadcasts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBroadcastBcastid(bcastid, callback)</td>
    <td style="padding:15px">Get broadcast object</td>
    <td style="padding:15px">{base_path}/{version}/broadcast/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBroadcastBcastid(bcastid, broadcast, callback)</td>
    <td style="padding:15px">Update a broadcast object</td>
    <td style="padding:15px">{base_path}/{version}/broadcast/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBroadcastBcastid(bcastid, callback)</td>
    <td style="padding:15px">Delete broadcast</td>
    <td style="padding:15px">{base_path}/{version}/broadcast/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApps(callback)</td>
    <td style="padding:15px">List Apps</td>
    <td style="padding:15px">{base_path}/{version}/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppAppid(appid, callback)</td>
    <td style="padding:15px">Get app</td>
    <td style="padding:15px">{base_path}/{version}/app/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomApps(callback)</td>
    <td style="padding:15px">List Custom Apps</td>
    <td style="padding:15px">{base_path}/{version}/custom_apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAppAppid(appid, callback)</td>
    <td style="padding:15px">Get app</td>
    <td style="padding:15px">{base_path}/{version}/custom_app/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomAppAppid(appid, callback)</td>
    <td style="padding:15px">Update a custom app.</td>
    <td style="padding:15px">{base_path}/{version}/custom_app/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomAppAppid(appid, callback)</td>
    <td style="padding:15px">Delete custom app</td>
    <td style="padding:15px">{base_path}/{version}/custom_app/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidCustomApps(orgid, callback)</td>
    <td style="padding:15px">Get Custom Apps</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/custom_apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCustomApps(orgid, customApp, callback)</td>
    <td style="padding:15px">Create custom application</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/custom_apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppGroups(callback)</td>
    <td style="padding:15px">List Application Groups</td>
    <td style="padding:15px">{base_path}/{version}/app_groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppGroupAppgrpid(appgrpid, callback)</td>
    <td style="padding:15px">Get Application Group</td>
    <td style="padding:15px">{base_path}/{version}/app_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAppGroupAppgrpid(appgrpid, callback)</td>
    <td style="padding:15px">Update a application group</td>
    <td style="padding:15px">{base_path}/{version}/app_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppGroupAppgrpid(appgrpid, callback)</td>
    <td style="padding:15px">Delete Application Group</td>
    <td style="padding:15px">{base_path}/{version}/app_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidAppGroups(orgid, callback)</td>
    <td style="padding:15px">Get Application Groups</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/app_groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidAppGroups(orgid, appgrp, callback)</td>
    <td style="padding:15px">Create application group</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/app_groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(callback)</td>
    <td style="padding:15px">List users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidUsers(orgid, callback)</td>
    <td style="padding:15px">Get user for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidUsers(orgid, user, callback)</td>
    <td style="padding:15px">Create user for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserUserid(userid, callback)</td>
    <td style="padding:15px">Get user</td>
    <td style="padding:15px">{base_path}/{version}/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUserUserid(userid, callback)</td>
    <td style="padding:15px">Update a user</td>
    <td style="padding:15px">{base_path}/{version}/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserUserid(userid, callback)</td>
    <td style="padding:15px">Delete user</td>
    <td style="padding:15px">{base_path}/{version}/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(callback)</td>
    <td style="padding:15px">List devices</td>
    <td style="padding:15px">{base_path}/{version}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDevid(devid, callback)</td>
    <td style="padding:15px">Get devices</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceDevid(devid, callback)</td>
    <td style="padding:15px">Update a device</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceDevid(devid, callback)</td>
    <td style="padding:15px">Delete device</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidDevices(orgid, callback)</td>
    <td style="padding:15px">Get device for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidDevices(orgid, device, callback)</td>
    <td style="padding:15px">Create device an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPathRules(callback)</td>
    <td style="padding:15px">List path rules</td>
    <td style="padding:15px">{base_path}/{version}/path_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPathRulePruleid(pruleid, callback)</td>
    <td style="padding:15px">Get path rules</td>
    <td style="padding:15px">{base_path}/{version}/path_rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPathRulePruleid(pruleid, callback)</td>
    <td style="padding:15px">Update a path rule</td>
    <td style="padding:15px">{base_path}/{version}/path_rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePathRulePruleid(pruleid, callback)</td>
    <td style="padding:15px">Delete path rule</td>
    <td style="padding:15px">{base_path}/{version}/path_rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidPathRules(orgid, callback)</td>
    <td style="padding:15px">Get path rule for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/path_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidPathRules(orgid, pathrule, callback)</td>
    <td style="padding:15px">Create path rule for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/path_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOutboundRules(callback)</td>
    <td style="padding:15px">List outbound rules</td>
    <td style="padding:15px">{base_path}/{version}/outbound_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOutboundRuleRuleid(ruleid, callback)</td>
    <td style="padding:15px">Get outbound rule</td>
    <td style="padding:15px">{base_path}/{version}/outbound_rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOutboundRuleRuleid(ruleid, callback)</td>
    <td style="padding:15px">Update a outbound rule</td>
    <td style="padding:15px">{base_path}/{version}/outbound_rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOutboundRuleRuleid(ruleid, callback)</td>
    <td style="padding:15px">Delete outbound rule</td>
    <td style="padding:15px">{base_path}/{version}/outbound_rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidOutboundRules(orgid, callback)</td>
    <td style="padding:15px">Get outbound rule for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/outbound_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidOutboundRules(orgid, outbound, callback)</td>
    <td style="padding:15px">Create outbound rule for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/outbound_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInboundRules(callback)</td>
    <td style="padding:15px">List inbound rules</td>
    <td style="padding:15px">{base_path}/{version}/inbound_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidInboundRules(orgid, callback)</td>
    <td style="padding:15px">Get inbound rules for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/inbound_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidInboundRules(orgid, inboundrule, callback)</td>
    <td style="padding:15px">Create inbound rule for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/inbound_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInboundRuleRuleid(ruleid, callback)</td>
    <td style="padding:15px">Get inbound rules</td>
    <td style="padding:15px">{base_path}/{version}/inbound_rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putInboundRuleRuleid(ruleid, callback)</td>
    <td style="padding:15px">Update a inbound rule</td>
    <td style="padding:15px">{base_path}/{version}/inbound_rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInboundRuleRuleid(ruleid, callback)</td>
    <td style="padding:15px">Delete inbound rule</td>
    <td style="padding:15px">{base_path}/{version}/inbound_rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEndpoints(callback)</td>
    <td style="padding:15px">List endpoints</td>
    <td style="padding:15px">{base_path}/{version}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidEndpoints(orgid, callback)</td>
    <td style="padding:15px">Get endpoints for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidEndpoints(orgid, endpoint, callback)</td>
    <td style="padding:15px">Create endpoint for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEndpointEpid(epid, callback)</td>
    <td style="padding:15px">Get endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEndpointEpid(epid, callback)</td>
    <td style="padding:15px">Update a endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEndpointEpid(epid, callback)</td>
    <td style="padding:15px">Delete endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworks(callback)</td>
    <td style="padding:15px">List networks</td>
    <td style="padding:15px">{base_path}/{version}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidNetworks(orgid, callback)</td>
    <td style="padding:15px">Get network for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidNetworks(orgid, network, callback)</td>
    <td style="padding:15px">Create network for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNetid(netid, callback)</td>
    <td style="padding:15px">Get network</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetworkNetid(netid, callback)</td>
    <td style="padding:15px">Update a network</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkNetid(netid, callback)</td>
    <td style="padding:15px">Delete network</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPorts(callback)</td>
    <td style="padding:15px">List Ports</td>
    <td style="padding:15px">{base_path}/{version}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortPortid(portid, callback)</td>
    <td style="padding:15px">Get port</td>
    <td style="padding:15px">{base_path}/{version}/port/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPortPortid(portid, callback)</td>
    <td style="padding:15px">Update a port</td>
    <td style="padding:15px">{base_path}/{version}/port/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeNodeidPorts(nodeid, callback)</td>
    <td style="padding:15px">Get port</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWans(callback)</td>
    <td style="padding:15px">List wans</td>
    <td style="padding:15px">{base_path}/{version}/wans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidWans(orgid, callback)</td>
    <td style="padding:15px">Get wan for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/wans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidWans(orgid, wan, callback)</td>
    <td style="padding:15px">Create wan for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/wans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWanWanid(wanid, callback)</td>
    <td style="padding:15px">Get wan</td>
    <td style="padding:15px">{base_path}/{version}/wan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWanWanid(wanid, callback)</td>
    <td style="padding:15px">Update a wan</td>
    <td style="padding:15px">{base_path}/{version}/wan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWanWanid(wanid, callback)</td>
    <td style="padding:15px">Delete wan</td>
    <td style="padding:15px">{base_path}/{version}/wan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSshtunnelNodeid(nodeid, callback)</td>
    <td style="padding:15px">Establish an SSH tunnel to the given node</td>
    <td style="padding:15px">{base_path}/{version}/sshtunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSshtunnelNodeid(nodeid, callback)</td>
    <td style="padding:15px">Return status of the SSH tunnel to the given node</td>
    <td style="padding:15px">{base_path}/{version}/sshtunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSshtunnelNodeid(nodeid, callback)</td>
    <td style="padding:15px">Stop the SSH tunnel with the given node</td>
    <td style="padding:15px">{base_path}/{version}/sshtunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSshtunnel(callback)</td>
    <td style="padding:15px">Status of all currently established ssh tunnels</td>
    <td style="padding:15px">{base_path}/{version}/sshtunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProxyservices(callback)</td>
    <td style="padding:15px">Return all Proxy Services</td>
    <td style="padding:15px">{base_path}/{version}/proxyservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProxyserviceProxyserviceid(proxyserviceid, callback)</td>
    <td style="padding:15px">Fetch Proxy Service object</td>
    <td style="padding:15px">{base_path}/{version}/proxyservice/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProxyserviceProxyserviceid(proxyserviceid, callback)</td>
    <td style="padding:15px">Delete a Proxy Service object</td>
    <td style="padding:15px">{base_path}/{version}/proxyservice/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProxyserviceProxyserviceid(proxyserviceid, callback)</td>
    <td style="padding:15px">Change a Proxy Service object</td>
    <td style="padding:15px">{base_path}/{version}/proxyservice/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidProxyservices(orgid, callback)</td>
    <td style="padding:15px">Return all Proxy Services in this org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/proxyservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidProxyservices(orgid, callback)</td>
    <td style="padding:15px">Create a Proxy Service object</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/proxyservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidProxyserviceSettings(orgid, callback)</td>
    <td style="padding:15px">Return proxyservice settings for this org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/proxyservice/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidProxyserviceSettings(orgid, callback)</td>
    <td style="padding:15px">Set proxyservice settings for this org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/proxyservice/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidProxyserviceConfig(orgid, callback)</td>
    <td style="padding:15px">Return zScaler config in CSV format</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/proxyservice/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProxyserviceCommand(callback)</td>
    <td style="padding:15px">Execute a proxyservice command</td>
    <td style="padding:15px">{base_path}/{version}/proxyservice/command?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergrps(callback)</td>
    <td style="padding:15px">List of User Groups</td>
    <td style="padding:15px">{base_path}/{version}/usergrps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergrpUsergrpid(usergrpid, callback)</td>
    <td style="padding:15px">Get usergrp</td>
    <td style="padding:15px">{base_path}/{version}/usergrp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsergrpUsergrpid(usergrpid, callback)</td>
    <td style="padding:15px">Update a usergrp</td>
    <td style="padding:15px">{base_path}/{version}/usergrp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsergrpUsergrpid(usergrpid, callback)</td>
    <td style="padding:15px">Delete usergrp</td>
    <td style="padding:15px">{base_path}/{version}/usergrp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidUsergrps(orgid, callback)</td>
    <td style="padding:15px">List usergrps</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/usergrps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidUsergrps(orgid, usergrp, callback)</td>
    <td style="padding:15px">Create usergrp for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/usergrps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortal(callback)</td>
    <td style="padding:15px">Retrieve all portals</td>
    <td style="padding:15px">{base_path}/{version}/portal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortalPortalid(portalid, callback)</td>
    <td style="padding:15px">Retrieve specified portal</td>
    <td style="padding:15px">{base_path}/{version}/portal/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPortalPortalid(portalid, callback)</td>
    <td style="padding:15px">Update a portal</td>
    <td style="padding:15px">{base_path}/{version}/portal/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortalPortalid(portalid, callback)</td>
    <td style="padding:15px">Delete portal</td>
    <td style="padding:15px">{base_path}/{version}/portal/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidPortals(orgid, callback)</td>
    <td style="padding:15px">List portals</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/portals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidPortals(orgid, portal, callback)</td>
    <td style="padding:15px">Create portal</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/portals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPortalPortalidPortalLogo(portalid, callback)</td>
    <td style="padding:15px">Portal Logo</td>
    <td style="padding:15px">{base_path}/{version}/portal/{pathv1}/portal_logo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOspfnets(callback)</td>
    <td style="padding:15px">Retrieve all ospfnets</td>
    <td style="padding:15px">{base_path}/{version}/ospfnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOspfnetOspfnetid(ospfnetid, callback)</td>
    <td style="padding:15px">Retrieve specified ospfnet</td>
    <td style="padding:15px">{base_path}/{version}/ospfnet/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOspfnetOspfnetid(ospfnetid, callback)</td>
    <td style="padding:15px">Update a OSPF network</td>
    <td style="padding:15px">{base_path}/{version}/ospfnet/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOspfnetOspfnetid(ospfnetid, callback)</td>
    <td style="padding:15px">Delete OSPF Network</td>
    <td style="padding:15px">{base_path}/{version}/ospfnet/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidOspfnets(orgid, callback)</td>
    <td style="padding:15px">List ospfnets for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ospfnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidOspfnets(orgid, ospfnet, callback)</td>
    <td style="padding:15px">Create ospfnet</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ospfnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRollingUpgrades(callback)</td>
    <td style="padding:15px">List of schedules</td>
    <td style="padding:15px">{base_path}/{version}/rolling_upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRollingUpgradeScheduleid(scheduleid, callback)</td>
    <td style="padding:15px">Get rolling_upgrade</td>
    <td style="padding:15px">{base_path}/{version}/rolling_upgrade/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRollingUpgradeScheduleid(scheduleid, callback)</td>
    <td style="padding:15px">Update a rolling_upgrade</td>
    <td style="padding:15px">{base_path}/{version}/rolling_upgrade/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRollingUpgradeScheduleid(scheduleid, callback)</td>
    <td style="padding:15px">Delete rolling_upgrade</td>
    <td style="padding:15px">{base_path}/{version}/rolling_upgrade/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidRollingUpgrades(orgid, callback)</td>
    <td style="padding:15px">List rolling_upgrades</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/rolling_upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidRollingUpgrades(orgid, rollingUpgrade, callback)</td>
    <td style="padding:15px">Create rolling_upgrade for an org</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/rolling_upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWifiPlans(callback)</td>
    <td style="padding:15px">Retrieve all wifi_plans</td>
    <td style="padding:15px">{base_path}/{version}/wifi_plans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWifiPlanWifiPlanid(wifiPlanid, callback)</td>
    <td style="padding:15px">Retrieve specified wifi_plan</td>
    <td style="padding:15px">{base_path}/{version}/wifi_plan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWifiPlanWifiPlanid(wifiPlanid, callback)</td>
    <td style="padding:15px">Update a wifi_plans</td>
    <td style="padding:15px">{base_path}/{version}/wifi_plan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWifiPlanWifiPlanid(wifiPlanid, callback)</td>
    <td style="padding:15px">Delete wifi_plan</td>
    <td style="padding:15px">{base_path}/{version}/wifi_plan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidWifiPlans(orgid, callback)</td>
    <td style="padding:15px">List wifi_plans</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/wifi_plans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidWifiPlans(orgid, wifiPlan, callback)</td>
    <td style="padding:15px">Create wifi_plan</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/wifi_plans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCvpns(callback)</td>
    <td style="padding:15px">Retrieve all cvpns</td>
    <td style="padding:15px">{base_path}/{version}/cvpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCvpnCvpnid(cvpnid, callback)</td>
    <td style="padding:15px">Retrieve specified cvpn</td>
    <td style="padding:15px">{base_path}/{version}/cvpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCvpnCvpnid(cvpnid, callback)</td>
    <td style="padding:15px">Update a cvpns</td>
    <td style="padding:15px">{base_path}/{version}/cvpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCvpnCvpnid(cvpnid, callback)</td>
    <td style="padding:15px">Delete cvpn</td>
    <td style="padding:15px">{base_path}/{version}/cvpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidCvpns(orgid, callback)</td>
    <td style="padding:15px">List cvpns</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cvpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCvpns(orgid, cvpn, callback)</td>
    <td style="padding:15px">Create cvpn</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cvpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCvpnCvpnidNetid(cvpnid, netid, callback)</td>
    <td style="padding:15px">Retrieve specified Local Net in CVPN.</td>
    <td style="padding:15px">{base_path}/{version}/cvpn/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCvpnCvpnidNetid(cvpnid, netid, callback)</td>
    <td style="padding:15px">Update a local Net</td>
    <td style="padding:15px">{base_path}/{version}/cvpn/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCvpnCvpnidNetid(cvpnid, netid, callback)</td>
    <td style="padding:15px">Delete local net on cvpn</td>
    <td style="padding:15px">{base_path}/{version}/cvpn/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAreas(callback)</td>
    <td style="padding:15px">Retrieve all areas</td>
    <td style="padding:15px">{base_path}/{version}/areas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAreaAreaid(areaid, callback)</td>
    <td style="padding:15px">Retrieve specified area</td>
    <td style="padding:15px">{base_path}/{version}/area/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAreaAreaid(areaid, callback)</td>
    <td style="padding:15px">Update a areas</td>
    <td style="padding:15px">{base_path}/{version}/area/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAreaAreaid(areaid, callback)</td>
    <td style="padding:15px">Delete area</td>
    <td style="padding:15px">{base_path}/{version}/area/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidAreas(orgid, callback)</td>
    <td style="padding:15px">List areas</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/areas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidAreas(orgid, area, callback)</td>
    <td style="padding:15px">Create area</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/areas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealm(callback)</td>
    <td style="padding:15px">Retrieve the realm of SCM</td>
    <td style="padding:15px">{base_path}/{version}/realm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRealm(callback)</td>
    <td style="padding:15px">Update a realm</td>
    <td style="padding:15px">{base_path}/{version}/realm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getShprimaryShprimaryid(shprimaryid, callback)</td>
    <td style="padding:15px">Retrieve specified SteelHead primary interface</td>
    <td style="padding:15px">{base_path}/{version}/shprimary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putShprimaryShprimaryid(shprimaryid, shPrimaryIpv4, shPrimaryGwv4, net, callback)</td>
    <td style="padding:15px">Update a specified SteelHead primary interface</td>
    <td style="padding:15px">{base_path}/{version}/shprimary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInpathInpathid(inpathid, callback)</td>
    <td style="padding:15px">Retrieve specified SteelHead inpath interface</td>
    <td style="padding:15px">{base_path}/{version}/inpath/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putInpathInpathid(inpathid, ipv4, net, callback)</td>
    <td style="padding:15px">Update a specified SteelHead inpath interface</td>
    <td style="padding:15px">{base_path}/{version}/inpath/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCepolicies(callback)</td>
    <td style="padding:15px">Retrieve all cepolicies</td>
    <td style="padding:15px">{base_path}/{version}/cepolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCepoliciesCepolicyid(cepolicyid, callback)</td>
    <td style="padding:15px">Retrieve specified cepolicy</td>
    <td style="padding:15px">{base_path}/{version}/cepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCepoliciesCepolicyid(cepolicyid, cepolicy, callback)</td>
    <td style="padding:15px">Update a Cepolicy network</td>
    <td style="padding:15px">{base_path}/{version}/cepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCepoliciesCepolicyid(cepolicyid, callback)</td>
    <td style="padding:15px">Delete Cepolicy Network</td>
    <td style="padding:15px">{base_path}/{version}/cepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidCepolicy(orgid, callback)</td>
    <td style="padding:15px">List cepolicies for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cepolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCepolicy(orgid, cepolicy, callback)</td>
    <td style="padding:15px">Create cepolicy</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cepolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCefilters(callback)</td>
    <td style="padding:15px">Retrieve all cefilters</td>
    <td style="padding:15px">{base_path}/{version}/cefilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCefiltersCefilterid(cefilterid, callback)</td>
    <td style="padding:15px">Retrieve specified cefilter</td>
    <td style="padding:15px">{base_path}/{version}/cefilters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCefiltersCefilterid(cefilterid, cefilter, callback)</td>
    <td style="padding:15px">Update a Cefilter network</td>
    <td style="padding:15px">{base_path}/{version}/cefilters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCefiltersCefilterid(cefilterid, callback)</td>
    <td style="padding:15px">Delete Cefilter Network</td>
    <td style="padding:15px">{base_path}/{version}/cefilters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidCefilters(orgid, callback)</td>
    <td style="padding:15px">List cefilters for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cefilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCefilters(orgid, cefilter, callback)</td>
    <td style="padding:15px">Create cefilter</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cefilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixes(callback)</td>
    <td style="padding:15px">Retrieve all prefixes</td>
    <td style="padding:15px">{base_path}/{version}/prefixes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixesPrefixid(prefixid, callback)</td>
    <td style="padding:15px">Retrieve specified prefix</td>
    <td style="padding:15px">{base_path}/{version}/prefixes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPrefixesPrefixid(prefixid, prefix, callback)</td>
    <td style="padding:15px">Update a Prefix network</td>
    <td style="padding:15px">{base_path}/{version}/prefixes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePrefixesPrefixid(prefixid, callback)</td>
    <td style="padding:15px">Delete Prefix Network</td>
    <td style="padding:15px">{base_path}/{version}/prefixes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidPrefix(orgid, callback)</td>
    <td style="padding:15px">List prefixes for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/prefix?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidPrefix(orgid, prefix, callback)</td>
    <td style="padding:15px">Create prefix</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/prefix?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCeexportrules(callback)</td>
    <td style="padding:15px">Retrieve all ceexportrules</td>
    <td style="padding:15px">{base_path}/{version}/ceexportrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCeexportrulesCeexportruleid(ceexportruleid, callback)</td>
    <td style="padding:15px">Retrieve specified ceexportrule</td>
    <td style="padding:15px">{base_path}/{version}/ceexportrules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCeexportrulesCeexportruleid(ceexportruleid, ceexportrule, callback)</td>
    <td style="padding:15px">Update a Ceexportrule network</td>
    <td style="padding:15px">{base_path}/{version}/ceexportrules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCeexportrulesCeexportruleid(ceexportruleid, callback)</td>
    <td style="padding:15px">Delete Ceexportrule Network</td>
    <td style="padding:15px">{base_path}/{version}/ceexportrules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidCeexportrules(orgid, callback)</td>
    <td style="padding:15px">List ceexportrules for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ceexportrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCeexportrules(orgid, ceexportrule, callback)</td>
    <td style="padding:15px">Create ceexportrule</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ceexportrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCeexportpolicies(callback)</td>
    <td style="padding:15px">Retrieve all ceexportpolicies</td>
    <td style="padding:15px">{base_path}/{version}/ceexportpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCeexportpoliciesCeexportpolicyid(ceexportpolicyid, callback)</td>
    <td style="padding:15px">Retrieve specified ceexportpolicy</td>
    <td style="padding:15px">{base_path}/{version}/ceexportpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCeexportpoliciesCeexportpolicyid(ceexportpolicyid, ceexportpolicy, callback)</td>
    <td style="padding:15px">Update a Ceexportpolicy network</td>
    <td style="padding:15px">{base_path}/{version}/ceexportpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCeexportpoliciesCeexportpolicyid(ceexportpolicyid, callback)</td>
    <td style="padding:15px">Delete Ceexportpolicy Network</td>
    <td style="padding:15px">{base_path}/{version}/ceexportpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidCeexportpolicies(orgid, callback)</td>
    <td style="padding:15px">List ceexportpolicies for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ceexportpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCeexportpolicies(orgid, ceexportpolicy, callback)</td>
    <td style="padding:15px">Create ceexportpolicy</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ceexportpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCeimportpolicies(callback)</td>
    <td style="padding:15px">Retrieve all ceimportpolicies</td>
    <td style="padding:15px">{base_path}/{version}/ceimportpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCeimportpoliciesCeimportpolicyid(ceimportpolicyid, callback)</td>
    <td style="padding:15px">Retrieve specified ceimportpolicy</td>
    <td style="padding:15px">{base_path}/{version}/ceimportpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCeimportpoliciesCeimportpolicyid(ceimportpolicyid, ceimportpolicy, callback)</td>
    <td style="padding:15px">Update a Ceimportpolicy network</td>
    <td style="padding:15px">{base_path}/{version}/ceimportpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCeimportpoliciesCeimportpolicyid(ceimportpolicyid, callback)</td>
    <td style="padding:15px">Delete Ceimportpolicy Network</td>
    <td style="padding:15px">{base_path}/{version}/ceimportpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidCeimportpolicies(orgid, callback)</td>
    <td style="padding:15px">List ceimportpolicies for given organization</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ceimportpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCeimportpolicies(orgid, ceimportpolicy, callback)</td>
    <td style="padding:15px">Create ceimportpolicy</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/ceimportpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSiteidCloudDeploy(siteid, callback)</td>
    <td style="padding:15px">List details of a deployed cloud site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/cloud-deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSiteSiteidCloudDeploy(siteid, cloudDeploy, callback)</td>
    <td style="padding:15px">Deploy a cloud site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/cloud-deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSiteSiteidCloudDeploy(siteid, cloudDeploy, callback)</td>
    <td style="padding:15px">Manage a cloud site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/cloud-deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteSiteidCloudDeploy(siteid, callback)</td>
    <td style="padding:15px">Undeploy a cloud site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/cloud-deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCloudAccounts(orgid, cloudAccount, callback)</td>
    <td style="padding:15px">Create a new account</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cloud-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidCloudAccounts(orgid, callback)</td>
    <td style="padding:15px">Get a list of accounts</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cloud-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCloudAccountAccountId(cloudAccount, accountId, callback)</td>
    <td style="padding:15px">Edit an existing account</td>
    <td style="padding:15px">{base_path}/{version}/cloud-account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudAccountAccountId(accountId, callback)</td>
    <td style="padding:15px">Delete an existing account.</td>
    <td style="padding:15px">{base_path}/{version}/cloud-account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudAccountAccountId(accountId, callback)</td>
    <td style="padding:15px">Get information about an existing account</td>
    <td style="padding:15px">{base_path}/{version}/cloud-account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudAccountAccountIdRefresh(accountId, callback)</td>
    <td style="padding:15px">Refresh Inventories for the a cloud account</td>
    <td style="padding:15px">{base_path}/{version}/cloud-account/{pathv1}/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgOrgidCloudAccountAccountIdCloudConnectSubnets(orgid, accountId, callback)</td>
    <td style="padding:15px">Get a list of subnets</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cloud-account/{pathv2}/cloud-connect/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId(orgid, accountId, subnetId, overrideOverlap, callback)</td>
    <td style="padding:15px">Connect a cloud subnet</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cloud-account/{pathv2}/cloud-connect/subnet/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId(orgid, accountId, subnetId, callback)</td>
    <td style="padding:15px">Disconnect a cloud subnet</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cloud-account/{pathv2}/cloud-connect/subnet/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId(orgid, accountId, vpcId, overrideOverlap, callback)</td>
    <td style="padding:15px">Connect all subnets under a vpc</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cloud-account/{pathv2}/cloud-connect/vpc/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId(orgid, accountId, vpcId, callback)</td>
    <td style="padding:15px">Disconnect all subnets under a vpc</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cloud-account/{pathv2}/cloud-connect/vpc/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId(orgid, accountId, vnetId, overrideOverlap, callback)</td>
    <td style="padding:15px">Connect all subnets under a vnet</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cloud-account/{pathv2}/cloud-connect/vnet/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId(orgid, accountId, vnetId, callback)</td>
    <td style="padding:15px">Disconnect all subnets under a vnet</td>
    <td style="padding:15px">{base_path}/{version}/org/{pathv1}/cloud-account/{pathv2}/cloud-connect/vnet/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
